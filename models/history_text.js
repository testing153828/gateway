const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('history_text', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'history_text',
    timestamps: false,
    indexes: [
      {
        name: "history_text_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "clock" },
        ]
      },
    ]
  });
};
