const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trends', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value_min: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    value_avg: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    value_max: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'trends',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "clock" },
        ]
      },
    ]
  });
};
