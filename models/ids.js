const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ids', {
    table_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    field_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    nextid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'ids',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "table_name" },
          { name: "field_name" },
        ]
      },
    ]
  });
};
