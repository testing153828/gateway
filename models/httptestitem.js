const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('httptestitem', {
    httptestitemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    httptestid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'httptest',
        key: 'httptestid'
      }
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'httptestitem',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httptestitemid" },
        ]
      },
      {
        name: "httptestitem_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httptestid" },
          { name: "itemid" },
        ]
      },
      {
        name: "httptestitem_2",
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
    ]
  });
};
