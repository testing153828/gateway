const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('config', {
    configid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    work_period: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "1-5,09:00-18:00"
    },
    alert_usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    default_theme: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: "blue-theme"
    },
    authentication_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ldap_host: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ldap_port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 389
    },
    ldap_base_dn: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ldap_bind_dn: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ldap_bind_password: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    ldap_search_attribute: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    discovery_groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    },
    max_in_table: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 50
    },
    search_limit: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1000
    },
    severity_color_0: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "97AAB3"
    },
    severity_color_1: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "7499FF"
    },
    severity_color_2: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "FFC859"
    },
    severity_color_3: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "FFA059"
    },
    severity_color_4: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "E97659"
    },
    severity_color_5: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "E45959"
    },
    severity_name_0: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "Not classified"
    },
    severity_name_1: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "Information"
    },
    severity_name_2: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "Warning"
    },
    severity_name_3: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "Average"
    },
    severity_name_4: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "High"
    },
    severity_name_5: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "Disaster"
    },
    ok_period: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "5m"
    },
    blink_period: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "2m"
    },
    problem_unack_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "CC0000"
    },
    problem_ack_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "CC0000"
    },
    ok_unack_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "009900"
    },
    ok_ack_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "009900"
    },
    problem_unack_style: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    problem_ack_style: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    ok_unack_style: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    ok_ack_style: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    snmptrap_logging: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    server_check_interval: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 10
    },
    hk_events_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_events_trigger: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "365d"
    },
    hk_events_internal: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "1d"
    },
    hk_events_discovery: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "1d"
    },
    hk_events_autoreg: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "1d"
    },
    hk_services_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_services: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "365d"
    },
    hk_audit_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_audit: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "365d"
    },
    hk_sessions_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_sessions: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "365d"
    },
    hk_history_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_history_global: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    hk_history: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "90d"
    },
    hk_trends_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    hk_trends_global: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    hk_trends: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "365d"
    },
    default_inventory_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    custom_color: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    http_auth_enabled: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    http_login_form: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    http_strip_domains: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    http_case_sensitive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    ldap_configured: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ldap_case_sensitive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    db_extension: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    },
    autoreg_tls_accept: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    compression_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    compress_older: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "7d"
    },
    instanceid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    },
    saml_auth_enabled: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_idp_entityid: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    saml_sso_url: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    saml_slo_url: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    saml_username_attribute: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    saml_sp_entityid: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    saml_nameid_format: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    saml_sign_messages: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_sign_assertions: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_sign_authn_requests: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_sign_logout_requests: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_sign_logout_responses: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_encrypt_nameid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_encrypt_assertions: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    saml_case_sensitive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    default_lang: {
      type: DataTypes.STRING(5),
      allowNull: false,
      defaultValue: "en_GB"
    },
    default_timezone: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "system"
    },
    login_attempts: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 5
    },
    login_block: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "30s"
    },
    show_technical_errors: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    validate_uri_schemes: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    uri_valid_schemes: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "http,https,ftp,file,mailto,tel,ssh"
    },
    x_frame_options: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "SAMEORIGIN"
    },
    iframe_sandboxing_enabled: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    iframe_sandboxing_exceptions: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    max_overview_table_size: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 50
    },
    history_period: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "24h"
    },
    period_default: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "1h"
    },
    max_period: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "2y"
    },
    socket_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "3s"
    },
    connect_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "3s"
    },
    media_type_test_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "65s"
    },
    script_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "60s"
    },
    item_test_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "60s"
    },
    session_key: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    report_test_timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "60s"
    },
    dbversion_status: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'config',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "configid" },
        ]
      },
      {
        name: "config_1",
        using: "BTREE",
        fields: [
          { name: "alert_usrgrpid" },
        ]
      },
      {
        name: "config_2",
        using: "BTREE",
        fields: [
          { name: "discovery_groupid" },
        ]
      },
    ]
  });
};
