const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profiles', {
    profileid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    idx: {
      type: DataTypes.STRING(96),
      allowNull: false,
      defaultValue: ""
    },
    idx2: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    value_id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    value_int: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value_str: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    source: {
      type: DataTypes.STRING(96),
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'profiles',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "profileid" },
        ]
      },
      {
        name: "profiles_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
          { name: "idx" },
          { name: "idx2" },
        ]
      },
      {
        name: "profiles_2",
        using: "BTREE",
        fields: [
          { name: "userid" },
          { name: "profileid" },
        ]
      },
    ]
  });
};
