const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proxy_autoreg_host', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    host: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    listen_ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    listen_port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    listen_dns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    host_metadata: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    tls_accepted: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    sequelize,
    tableName: 'proxy_autoreg_host',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "proxy_autoreg_host_1",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
    ]
  });
};
