const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('interface_discovery', {
    interfaceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'interface',
        key: 'interfaceid'
      }
    },
    parent_interfaceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'interface',
        key: 'interfaceid'
      }
    }
  }, {
    sequelize,
    tableName: 'interface_discovery',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "interfaceid" },
        ]
      },
      {
        name: "c_interface_discovery_2",
        using: "BTREE",
        fields: [
          { name: "parent_interfaceid" },
        ]
      },
    ]
  });
};
