const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('monitoring_history', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Company_ID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Company_Name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Company_RecID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Sku: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Qty: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    month: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    device: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ItemName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Monitor: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Monitor_Company_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'monitoring_history',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "unique_index",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "device" },
          { name: "Sku" },
          { name: "month" },
          { name: "year" },
          { name: "ItemName" },
        ]
      },
      {
        name: "Company_ID",
        using: "BTREE",
        fields: [
          { name: "Company_ID" },
        ]
      },
      {
        name: "Company_RecID",
        using: "BTREE",
        fields: [
          { name: "Company_RecID" },
        ]
      },
      {
        name: "Sku",
        using: "BTREE",
        fields: [
          { name: "Sku" },
        ]
      },
      {
        name: "month",
        using: "BTREE",
        fields: [
          { name: "month" },
        ]
      },
      {
        name: "year",
        using: "BTREE",
        fields: [
          { name: "year" },
        ]
      },
      {
        name: "device",
        using: "BTREE",
        fields: [
          { name: "device" },
        ]
      },
    ]
  });
};
