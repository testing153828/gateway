const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('widget', {
    widgetid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    x: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    y: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    view_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dashboard_pageid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dashboard_page',
        key: 'dashboard_pageid'
      }
    }
  }, {
    sequelize,
    tableName: 'widget',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "widgetid" },
        ]
      },
      {
        name: "widget_1",
        using: "BTREE",
        fields: [
          { name: "dashboard_pageid" },
        ]
      },
    ]
  });
};
