const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_optemplate', {
    lld_override_optemplateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    lld_override_operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'lld_override_operation',
        key: 'lld_override_operationid'
      }
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    }
  }, {
    sequelize,
    tableName: 'lld_override_optemplate',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_optemplateid" },
        ]
      },
      {
        name: "lld_override_optemplate_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_operationid" },
          { name: "templateid" },
        ]
      },
      {
        name: "lld_override_optemplate_2",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
