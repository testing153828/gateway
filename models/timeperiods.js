const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('timeperiods', {
    timeperiodid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    timeperiod_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    every: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    month: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dayofweek: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    day: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    start_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    period: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    start_date: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'timeperiods',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "timeperiodid" },
        ]
      },
    ]
  });
};
