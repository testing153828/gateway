const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('acknowledges', {
    acknowledgeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    message: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    action: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    old_severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    new_severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'acknowledges',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "acknowledgeid" },
        ]
      },
      {
        name: "acknowledges_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "acknowledges_2",
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "acknowledges_3",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
    ]
  });
};
