const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report_user', {
    reportuserid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    reportid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'report',
        key: 'reportid'
      }
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    exclude: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    access_userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'users',
        key: 'userid'
      }
    }
  }, {
    sequelize,
    tableName: 'report_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "reportuserid" },
        ]
      },
      {
        name: "report_user_1",
        using: "BTREE",
        fields: [
          { name: "reportid" },
        ]
      },
      {
        name: "c_report_user_2",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "c_report_user_3",
        using: "BTREE",
        fields: [
          { name: "access_userid" },
        ]
      },
    ]
  });
};
