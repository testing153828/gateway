const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opcommand_grp', {
    opcommand_grpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    }
  }, {
    sequelize,
    tableName: 'opcommand_grp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "opcommand_grpid" },
        ]
      },
      {
        name: "opcommand_grp_1",
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
      {
        name: "opcommand_grp_2",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
