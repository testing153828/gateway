const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('task_result', {
    taskid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'task',
        key: 'taskid'
      }
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    parent_taskid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    info: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'task_result',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "taskid" },
        ]
      },
      {
        name: "task_result_1",
        using: "BTREE",
        fields: [
          { name: "parent_taskid" },
        ]
      },
    ]
  });
};
