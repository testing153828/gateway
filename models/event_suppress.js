const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('event_suppress', {
    event_suppressid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'maintenances',
        key: 'maintenanceid'
      }
    },
    suppress_until: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'event_suppress',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "event_suppressid" },
        ]
      },
      {
        name: "event_suppress_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "eventid" },
          { name: "maintenanceid" },
        ]
      },
      {
        name: "event_suppress_2",
        using: "BTREE",
        fields: [
          { name: "suppress_until" },
        ]
      },
      {
        name: "event_suppress_3",
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
        ]
      },
    ]
  });
};
