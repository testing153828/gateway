const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('services_times', {
    timeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    serviceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'services',
        key: 'serviceid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ts_from: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ts_to: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    note: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'services_times',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "timeid" },
        ]
      },
      {
        name: "services_times_1",
        using: "BTREE",
        fields: [
          { name: "serviceid" },
          { name: "type" },
          { name: "ts_from" },
          { name: "ts_to" },
        ]
      },
    ]
  });
};
