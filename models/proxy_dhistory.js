const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('proxy_dhistory', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    druleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dcheckid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    dns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'proxy_dhistory',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "proxy_dhistory_1",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
      {
        name: "proxy_dhistory_2",
        using: "BTREE",
        fields: [
          { name: "druleid" },
        ]
      },
    ]
  });
};
