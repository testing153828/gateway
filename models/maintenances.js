const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenances', {
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: "",
      unique: "maintenances_2"
    },
    maintenance_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    active_since: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    active_till: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    tags_evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'maintenances',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
        ]
      },
      {
        name: "maintenances_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "maintenances_1",
        using: "BTREE",
        fields: [
          { name: "active_since" },
          { name: "active_till" },
        ]
      },
    ]
  });
};
