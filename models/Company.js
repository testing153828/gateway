const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Company', {
    Company_RecID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "Primary Key"
    },
    Company_ID: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Company_Name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Date_Entered: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Userfield_1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_6: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_7: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_8: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_9: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    Userfield_10: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'Company',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Company_RecID" },
        ]
      },
    ]
  });
};
