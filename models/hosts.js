const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hosts', {
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    proxy_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    host: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastaccess: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ipmi_authtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    ipmi_privilege: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    ipmi_username: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ""
    },
    ipmi_password: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: ""
    },
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'maintenances',
        key: 'maintenanceid'
      }
    },
    maintenance_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    maintenance_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    maintenance_from: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tls_connect: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    tls_accept: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    tls_issuer: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    tls_subject: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    tls_psk_identity: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    tls_psk: {
      type: DataTypes.STRING(512),
      allowNull: false,
      defaultValue: ""
    },
    proxy_address: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    auto_compress: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    discover: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    custom_interfaces: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'hosts',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
      {
        name: "hosts_1",
        using: "BTREE",
        fields: [
          { name: "host" },
        ]
      },
      {
        name: "hosts_2",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "hosts_3",
        using: "BTREE",
        fields: [
          { name: "proxy_hostid" },
        ]
      },
      {
        name: "hosts_4",
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "hosts_5",
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
        ]
      },
      {
        name: "c_hosts_3",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
