const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rights', {
    rightid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    permission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    }
  }, {
    sequelize,
    tableName: 'rights',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "rightid" },
        ]
      },
      {
        name: "rights_1",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
      {
        name: "rights_2",
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
