const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opcommand_hst', {
    opcommand_hstid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    }
  }, {
    sequelize,
    tableName: 'opcommand_hst',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "opcommand_hstid" },
        ]
      },
      {
        name: "opcommand_hst_1",
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
      {
        name: "opcommand_hst_2",
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
    ]
  });
};
