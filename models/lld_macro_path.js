const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_macro_path', {
    lld_macro_pathid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    lld_macro: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    path: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'lld_macro_path',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_macro_pathid" },
        ]
      },
      {
        name: "lld_macro_path_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "lld_macro" },
        ]
      },
    ]
  });
};
