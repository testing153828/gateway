const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('triggers', {
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    expression: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    priority: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastchange: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    error: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    recovery_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    recovery_expression: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    correlation_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    correlation_tag: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    manual_close: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    opdata: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    discover: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    event_name: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'triggers',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
      {
        name: "triggers_1",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "triggers_2",
        using: "BTREE",
        fields: [
          { name: "value" },
          { name: "lastchange" },
        ]
      },
      {
        name: "triggers_3",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
