const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('graphs', {
    graphid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 900
    },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 200
    },
    yaxismin: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    yaxismax: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 100
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'graphs',
        key: 'graphid'
      }
    },
    show_work_period: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    show_triggers: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    graphtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    show_legend: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    show_3d: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    percent_left: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    percent_right: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    ymin_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ymax_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ymin_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    ymax_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    discover: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'graphs',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "graphid" },
        ]
      },
      {
        name: "graphs_1",
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "graphs_2",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
      {
        name: "graphs_3",
        using: "BTREE",
        fields: [
          { name: "ymin_itemid" },
        ]
      },
      {
        name: "graphs_4",
        using: "BTREE",
        fields: [
          { name: "ymax_itemid" },
        ]
      },
    ]
  });
};
