const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hosts_groups', {
    hostgroupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    }
  }, {
    sequelize,
    tableName: 'hosts_groups',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostgroupid" },
        ]
      },
      {
        name: "hosts_groups_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostid" },
          { name: "groupid" },
        ]
      },
      {
        name: "hosts_groups_2",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
