const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('housekeeper', {
    housekeeperid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    tablename: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    field: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'housekeeper',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "housekeeperid" },
        ]
      },
    ]
  });
};
