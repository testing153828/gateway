const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('corr_condition_tagpair', {
    corr_conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'corr_condition',
        key: 'corr_conditionid'
      }
    },
    oldtag: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    newtag: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'corr_condition_tagpair',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "corr_conditionid" },
        ]
      },
    ]
  });
};
