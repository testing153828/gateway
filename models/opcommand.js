const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opcommand', {
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    scriptid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'scripts',
        key: 'scriptid'
      }
    }
  }, {
    sequelize,
    tableName: 'opcommand',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
      {
        name: "opcommand_1",
        using: "BTREE",
        fields: [
          { name: "scriptid" },
        ]
      },
    ]
  });
};
