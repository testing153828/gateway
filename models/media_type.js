const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('media_type', {
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: "",
      unique: "media_type_1"
    },
    smtp_server: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    smtp_helo: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    smtp_email: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    exec_path: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    gsm_modem: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    passwd: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    smtp_port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 25
    },
    smtp_security: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    smtp_verify_peer: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    smtp_verify_host: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    smtp_authentication: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    exec_params: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    maxsessions: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    maxattempts: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 3
    },
    attempt_interval: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "10s"
    },
    content_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    script: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "30s"
    },
    process_tags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    show_event_menu: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    event_menu_url: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    event_menu_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'media_type',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
        ]
      },
      {
        name: "media_type_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
};
