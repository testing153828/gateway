const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenances_hosts', {
    maintenance_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'maintenances',
        key: 'maintenanceid'
      }
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    }
  }, {
    sequelize,
    tableName: 'maintenances_hosts',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenance_hostid" },
        ]
      },
      {
        name: "maintenances_hosts_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
          { name: "hostid" },
        ]
      },
      {
        name: "maintenances_hosts_2",
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
    ]
  });
};
