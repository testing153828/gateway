const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dservices', {
    dserviceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    dhostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dhosts',
        key: 'dhostid'
      }
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastup: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastdown: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dcheckid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dchecks',
        key: 'dcheckid'
      }
    },
    ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    dns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'dservices',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dserviceid" },
        ]
      },
      {
        name: "dservices_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dcheckid" },
          { name: "ip" },
          { name: "port" },
        ]
      },
      {
        name: "dservices_2",
        using: "BTREE",
        fields: [
          { name: "dhostid" },
        ]
      },
    ]
  });
};
