const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_operation', {
    lld_override_operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    lld_overrideid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'lld_override',
        key: 'lld_overrideid'
      }
    },
    operationobject: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'lld_override_operation',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_operationid" },
        ]
      },
      {
        name: "lld_override_operation_1",
        using: "BTREE",
        fields: [
          { name: "lld_overrideid" },
        ]
      },
    ]
  });
};
