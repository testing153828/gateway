const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('problem', {
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    source: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    object: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    objectid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    r_eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    r_clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    r_ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    correlationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    acknowledged: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'problem',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "problem_1",
        using: "BTREE",
        fields: [
          { name: "source" },
          { name: "object" },
          { name: "objectid" },
        ]
      },
      {
        name: "problem_2",
        using: "BTREE",
        fields: [
          { name: "r_clock" },
        ]
      },
      {
        name: "problem_3",
        using: "BTREE",
        fields: [
          { name: "r_eventid" },
        ]
      },
    ]
  });
};
