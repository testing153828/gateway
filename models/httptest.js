const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('httptest', {
    httptestid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    nextcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    delay: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "1m"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    agent: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "Zabbix"
    },
    authentication: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    http_user: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    http_password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'httptest',
        key: 'httptestid'
      }
    },
    http_proxy: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    retries: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    ssl_cert_file: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ssl_key_file: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ssl_key_password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    verify_peer: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    verify_host: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'httptest',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httptestid" },
        ]
      },
      {
        name: "httptest_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostid" },
          { name: "name" },
        ]
      },
      {
        name: "httptest_3",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "httptest_4",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
