const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmap_usrgrp', {
    sysmapusrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    },
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    permission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    }
  }, {
    sequelize,
    tableName: 'sysmap_usrgrp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmapusrgrpid" },
        ]
      },
      {
        name: "sysmap_usrgrp_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmapid" },
          { name: "usrgrpid" },
        ]
      },
      {
        name: "c_sysmap_usrgrp_2",
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
    ]
  });
};
