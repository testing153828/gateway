const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmap_user', {
    sysmapuserid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    permission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    }
  }, {
    sequelize,
    tableName: 'sysmap_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmapuserid" },
        ]
      },
      {
        name: "sysmap_user_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmapid" },
          { name: "userid" },
        ]
      },
      {
        name: "c_sysmap_user_2",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
    ]
  });
};
