const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dashboard', {
    dashboardid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    private: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    display_period: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 30
    },
    auto_start: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'dashboard',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dashboardid" },
        ]
      },
      {
        name: "dashboard_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "dashboard_2",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
