const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trigger_discovery', {
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    parent_triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    lastcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ts_delete: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'trigger_discovery',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
      {
        name: "trigger_discovery_1",
        using: "BTREE",
        fields: [
          { name: "parent_triggerid" },
        ]
      },
    ]
  });
};
