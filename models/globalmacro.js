const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('globalmacro', {
    globalmacroid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    macro: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      unique: "globalmacro_1"
    },
    value: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'globalmacro',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "globalmacroid" },
        ]
      },
      {
        name: "globalmacro_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "macro" },
        ]
      },
    ]
  });
};
