const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('item_preproc', {
    item_preprocid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    step: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    params: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    error_handler: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    error_handler_params: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'item_preproc',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "item_preprocid" },
        ]
      },
      {
        name: "item_preproc_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "step" },
        ]
      },
    ]
  });
};
