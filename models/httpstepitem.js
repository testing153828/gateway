const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('httpstepitem', {
    httpstepitemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    httpstepid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'httpstep',
        key: 'httpstepid'
      }
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'httpstepitem',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httpstepitemid" },
        ]
      },
      {
        name: "httpstepitem_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httpstepid" },
          { name: "itemid" },
        ]
      },
      {
        name: "httpstepitem_2",
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
    ]
  });
};
