const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report_usrgrp', {
    reportusrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    reportid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'report',
        key: 'reportid'
      }
    },
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    access_userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'users',
        key: 'userid'
      }
    }
  }, {
    sequelize,
    tableName: 'report_usrgrp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "reportusrgrpid" },
        ]
      },
      {
        name: "report_usrgrp_1",
        using: "BTREE",
        fields: [
          { name: "reportid" },
        ]
      },
      {
        name: "c_report_usrgrp_2",
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
      {
        name: "c_report_usrgrp_3",
        using: "BTREE",
        fields: [
          { name: "access_userid" },
        ]
      },
    ]
  });
};
