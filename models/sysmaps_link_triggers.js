const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmaps_link_triggers', {
    linktriggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    linkid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps_links',
        key: 'linkid'
      }
    },
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    drawtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "000000"
    }
  }, {
    sequelize,
    tableName: 'sysmaps_link_triggers',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "linktriggerid" },
        ]
      },
      {
        name: "sysmaps_link_triggers_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "linkid" },
          { name: "triggerid" },
        ]
      },
      {
        name: "sysmaps_link_triggers_2",
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
    ]
  });
};
