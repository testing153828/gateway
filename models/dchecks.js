const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dchecks', {
    dcheckid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    druleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'drules',
        key: 'druleid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    key_: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    snmp_community: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ports: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0"
    },
    snmpv3_securityname: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    snmpv3_securitylevel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    snmpv3_authpassphrase: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    snmpv3_privpassphrase: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    uniq: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    snmpv3_authprotocol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    snmpv3_privprotocol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    snmpv3_contextname: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    host_source: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    name_source: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'dchecks',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dcheckid" },
        ]
      },
      {
        name: "dchecks_1",
        using: "BTREE",
        fields: [
          { name: "druleid" },
          { name: "host_source" },
          { name: "name_source" },
        ]
      },
    ]
  });
};
