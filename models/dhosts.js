const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dhosts', {
    dhostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    druleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'drules',
        key: 'druleid'
      }
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastup: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastdown: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'dhosts',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dhostid" },
        ]
      },
      {
        name: "dhosts_1",
        using: "BTREE",
        fields: [
          { name: "druleid" },
        ]
      },
    ]
  });
};
