const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenances_groups', {
    maintenance_groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'maintenances',
        key: 'maintenanceid'
      }
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    }
  }, {
    sequelize,
    tableName: 'maintenances_groups',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenance_groupid" },
        ]
      },
      {
        name: "maintenances_groups_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
          { name: "groupid" },
        ]
      },
      {
        name: "maintenances_groups_2",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
