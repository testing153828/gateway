const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SKU', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    sku: {
      type: DataTypes.STRING(255),
      allowNull: true,
      unique: "sku"
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    customerDescription: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    unitCost: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: true
    },
    unitPrice: {
      type: DataTypes.DECIMAL(5,2),
      allowNull: true
    },
    hardCost: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    groupid: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'SKU_Groups',
        key: 'groupid'
      }
    },
    dateadded: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    dateupdated: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'SKU',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "sku",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sku" },
        ]
      },
      {
        name: "FK_groupid",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
