const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trigger_depends', {
    triggerdepid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    triggerid_down: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    triggerid_up: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    }
  }, {
    sequelize,
    tableName: 'trigger_depends',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "triggerdepid" },
        ]
      },
      {
        name: "trigger_depends_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "triggerid_down" },
          { name: "triggerid_up" },
        ]
      },
      {
        name: "trigger_depends_2",
        using: "BTREE",
        fields: [
          { name: "triggerid_up" },
        ]
      },
    ]
  });
};
