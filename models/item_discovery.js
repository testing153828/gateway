const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('item_discovery', {
    itemdiscoveryid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    parent_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    key_: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    lastcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ts_delete: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'item_discovery',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemdiscoveryid" },
        ]
      },
      {
        name: "item_discovery_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "parent_itemid" },
        ]
      },
      {
        name: "item_discovery_2",
        using: "BTREE",
        fields: [
          { name: "parent_itemid" },
        ]
      },
    ]
  });
};
