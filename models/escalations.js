const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('escalations', {
    escalationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    actionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    r_eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    nextcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    esc_step: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    acknowledgeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'escalations',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "escalationid" },
        ]
      },
      {
        name: "escalations_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "triggerid" },
          { name: "itemid" },
          { name: "escalationid" },
        ]
      },
      {
        name: "escalations_2",
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "escalations_3",
        using: "BTREE",
        fields: [
          { name: "nextcheck" },
        ]
      },
    ]
  });
};
