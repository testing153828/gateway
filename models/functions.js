const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('functions', {
    functionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    name: {
      type: DataTypes.STRING(12),
      allowNull: false,
      defaultValue: ""
    },
    parameter: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0"
    }
  }, {
    sequelize,
    tableName: 'functions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "functionid" },
        ]
      },
      {
        name: "functions_1",
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
      {
        name: "functions_2",
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "name" },
          { name: "parameter" },
        ]
      },
    ]
  });
};
