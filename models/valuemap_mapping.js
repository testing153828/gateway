const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('valuemap_mapping', {
    valuemap_mappingid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    valuemapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'valuemap',
        key: 'valuemapid'
      }
    },
    value: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    newvalue: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sortorder: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'valuemap_mapping',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "valuemap_mappingid" },
        ]
      },
      {
        name: "valuemap_mapping_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "valuemapid" },
          { name: "value" },
          { name: "type" },
        ]
      },
    ]
  });
};
