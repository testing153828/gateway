const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('httpstep', {
    httpstepid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    httptestid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'httptest',
        key: 'httptestid'
      }
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    no: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    url: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    timeout: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "15s"
    },
    posts: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    required: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    status_codes: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    follow_redirects: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    retrieve_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    post_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'httpstep',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "httpstepid" },
        ]
      },
      {
        name: "httpstep_1",
        using: "BTREE",
        fields: [
          { name: "httptestid" },
        ]
      },
    ]
  });
};
