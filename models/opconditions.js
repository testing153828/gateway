const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opconditions', {
    opconditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    conditiontype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'opconditions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "opconditionid" },
        ]
      },
      {
        name: "opconditions_1",
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
    ]
  });
};
