const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('expressions', {
    expressionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    regexpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'regexps',
        key: 'regexpid'
      }
    },
    expression: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    expression_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    exp_delimiter: {
      type: DataTypes.STRING(1),
      allowNull: false,
      defaultValue: ""
    },
    case_sensitive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'expressions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "expressionid" },
        ]
      },
      {
        name: "expressions_1",
        using: "BTREE",
        fields: [
          { name: "regexpid" },
        ]
      },
    ]
  });
};
