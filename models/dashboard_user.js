const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dashboard_user', {
    dashboard_userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    dashboardid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dashboard',
        key: 'dashboardid'
      }
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    permission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    }
  }, {
    sequelize,
    tableName: 'dashboard_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dashboard_userid" },
        ]
      },
      {
        name: "dashboard_user_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dashboardid" },
          { name: "userid" },
        ]
      },
      {
        name: "c_dashboard_user_2",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
    ]
  });
};
