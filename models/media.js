const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('media', {
    mediaid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'media_type',
        key: 'mediatypeid'
      }
    },
    sendto: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    active: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 63
    },
    period: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: "1-7,00:00-24:00"
    }
  }, {
    sequelize,
    tableName: 'media',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "mediaid" },
        ]
      },
      {
        name: "media_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "media_2",
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
        ]
      },
    ]
  });
};
