const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opmessage', {
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    default_msg: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    subject: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'media_type',
        key: 'mediatypeid'
      }
    }
  }, {
    sequelize,
    tableName: 'opmessage',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
      {
        name: "opmessage_1",
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
        ]
      },
    ]
  });
};
