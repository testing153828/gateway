const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('role_rule', {
    role_ruleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    roleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'role',
        key: 'roleid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value_int: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value_str: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value_moduleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'module',
        key: 'moduleid'
      }
    }
  }, {
    sequelize,
    tableName: 'role_rule',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "role_ruleid" },
        ]
      },
      {
        name: "role_rule_1",
        using: "BTREE",
        fields: [
          { name: "roleid" },
        ]
      },
      {
        name: "role_rule_2",
        using: "BTREE",
        fields: [
          { name: "value_moduleid" },
        ]
      },
    ]
  });
};
