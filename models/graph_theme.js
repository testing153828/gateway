const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('graph_theme', {
    graphthemeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    theme: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "",
      unique: "graph_theme_1"
    },
    backgroundcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    graphcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    gridcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    maingridcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    gridbordercolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    textcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    highlightcolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    leftpercentilecolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    rightpercentilecolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    nonworktimecolor: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    colorpalette: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'graph_theme',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "graphthemeid" },
        ]
      },
      {
        name: "graph_theme_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "theme" },
        ]
      },
    ]
  });
};
