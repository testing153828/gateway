const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('host_discovery', {
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    parent_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    parent_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    host: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    lastcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ts_delete: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'host_discovery',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
      {
        name: "c_host_discovery_2",
        using: "BTREE",
        fields: [
          { name: "parent_hostid" },
        ]
      },
      {
        name: "c_host_discovery_3",
        using: "BTREE",
        fields: [
          { name: "parent_itemid" },
        ]
      },
    ]
  });
};
