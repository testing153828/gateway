const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('operations', {
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    actionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'actions',
        key: 'actionid'
      }
    },
    operationtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    esc_period: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0"
    },
    esc_step_from: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    esc_step_to: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    recovery: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'operations',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
        ]
      },
      {
        name: "operations_1",
        using: "BTREE",
        fields: [
          { name: "actionid" },
        ]
      },
    ]
  });
};
