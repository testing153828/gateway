const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('auditlog_details', {
    auditdetailid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    auditid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'auditlog',
        key: 'auditid'
      }
    },
    table_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    field_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    oldvalue: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    newvalue: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'auditlog_details',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "auditdetailid" },
        ]
      },
      {
        name: "auditlog_details_1",
        using: "BTREE",
        fields: [
          { name: "auditid" },
        ]
      },
    ]
  });
};
