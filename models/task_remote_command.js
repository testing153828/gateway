const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('task_remote_command', {
    taskid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'task',
        key: 'taskid'
      }
    },
    command_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    execute_on: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    authtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    username: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    publickey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    privatekey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    command: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    alertid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    parent_taskid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'task_remote_command',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "taskid" },
        ]
      },
    ]
  });
};
