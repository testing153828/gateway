const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('script_param', {
    script_paramid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    scriptid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'scripts',
        key: 'scriptid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'script_param',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "script_paramid" },
        ]
      },
      {
        name: "script_param_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "scriptid" },
          { name: "name" },
        ]
      },
    ]
  });
};
