const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('events', {
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    source: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    object: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    objectid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    acknowledged: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'events',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "events_1",
        using: "BTREE",
        fields: [
          { name: "source" },
          { name: "object" },
          { name: "objectid" },
          { name: "clock" },
        ]
      },
      {
        name: "events_2",
        using: "BTREE",
        fields: [
          { name: "source" },
          { name: "object" },
          { name: "clock" },
        ]
      },
    ]
  });
};
