const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmap_element_url', {
    sysmapelementurlid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    selementid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps_elements',
        key: 'selementid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'sysmap_element_url',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmapelementurlid" },
        ]
      },
      {
        name: "sysmap_element_url_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "selementid" },
          { name: "name" },
        ]
      },
    ]
  });
};
