const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('interface_snmp', {
    interfaceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'interface',
        key: 'interfaceid'
      }
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    bulk: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    community: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    securityname: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    securitylevel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    authpassphrase: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    privpassphrase: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    authprotocol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    privprotocol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    contextname: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'interface_snmp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "interfaceid" },
        ]
      },
    ]
  });
};
