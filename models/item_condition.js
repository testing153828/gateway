const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('item_condition', {
    item_conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 8
    },
    macro: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'item_condition',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "item_conditionid" },
        ]
      },
      {
        name: "item_condition_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
    ]
  });
};
