const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('maintenances_windows', {
    maintenance_timeperiodid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    maintenanceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'maintenances',
        key: 'maintenanceid'
      }
    },
    timeperiodid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'timeperiods',
        key: 'timeperiodid'
      }
    }
  }, {
    sequelize,
    tableName: 'maintenances_windows',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenance_timeperiodid" },
        ]
      },
      {
        name: "maintenances_windows_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "maintenanceid" },
          { name: "timeperiodid" },
        ]
      },
      {
        name: "maintenances_windows_2",
        using: "BTREE",
        fields: [
          { name: "timeperiodid" },
        ]
      },
    ]
  });
};
