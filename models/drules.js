const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('drules', {
    druleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    proxy_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      unique: "drules_2"
    },
    iprange: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    delay: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "1h"
    },
    nextcheck: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'drules',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "druleid" },
        ]
      },
      {
        name: "drules_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "drules_1",
        using: "BTREE",
        fields: [
          { name: "proxy_hostid" },
        ]
      },
    ]
  });
};
