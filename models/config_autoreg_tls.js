const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('config_autoreg_tls', {
    autoreg_tlsid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    tls_psk_identity: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: "",
      unique: "config_autoreg_tls_1"
    },
    tls_psk: {
      type: DataTypes.STRING(512),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'config_autoreg_tls',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "autoreg_tlsid" },
        ]
      },
      {
        name: "config_autoreg_tls_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tls_psk_identity" },
        ]
      },
    ]
  });
};
