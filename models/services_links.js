const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('services_links', {
    linkid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    serviceupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'services',
        key: 'serviceid'
      }
    },
    servicedownid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'services',
        key: 'serviceid'
      }
    },
    soft: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'services_links',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "linkid" },
        ]
      },
      {
        name: "services_links_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "serviceupid" },
          { name: "servicedownid" },
        ]
      },
      {
        name: "services_links_1",
        using: "BTREE",
        fields: [
          { name: "servicedownid" },
        ]
      },
    ]
  });
};
