const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('task', {
    taskid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ttl: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    proxy_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    }
  }, {
    sequelize,
    tableName: 'task',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "taskid" },
        ]
      },
      {
        name: "task_1",
        using: "BTREE",
        fields: [
          { name: "status" },
          { name: "proxy_hostid" },
        ]
      },
      {
        name: "c_task_1",
        using: "BTREE",
        fields: [
          { name: "proxy_hostid" },
        ]
      },
    ]
  });
};
