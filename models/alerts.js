const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('alerts', {
    alertid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    actionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'actions',
        key: 'actionid'
      }
    },
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'media_type',
        key: 'mediatypeid'
      }
    },
    sendto: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: ""
    },
    subject: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    retries: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    error: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    esc_step: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    alerttype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    p_eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    acknowledgeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'acknowledges',
        key: 'acknowledgeid'
      }
    },
    parameters: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'alerts',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "alertid" },
        ]
      },
      {
        name: "alerts_1",
        using: "BTREE",
        fields: [
          { name: "actionid" },
        ]
      },
      {
        name: "alerts_2",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
      {
        name: "alerts_3",
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "alerts_4",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "alerts_5",
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
        ]
      },
      {
        name: "alerts_6",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "alerts_7",
        using: "BTREE",
        fields: [
          { name: "p_eventid" },
        ]
      },
      {
        name: "c_alerts_6",
        using: "BTREE",
        fields: [
          { name: "acknowledgeid" },
        ]
      },
      {
        name: "alerts_8",
        using: "BTREE",
        fields: [
          { name: "acknowledgeid" },
        ]
      },
    ]
  });
};
