const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmaps_elements', {
    selementid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    },
    elementid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    elementtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    iconid_off: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'images',
        key: 'imageid'
      }
    },
    iconid_on: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'images',
        key: 'imageid'
      }
    },
    label: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    label_location: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: -1
    },
    x: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    y: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    iconid_disabled: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'images',
        key: 'imageid'
      }
    },
    iconid_maintenance: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'images',
        key: 'imageid'
      }
    },
    elementsubtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    areatype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 200
    },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 200
    },
    viewtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    use_iconmap: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'sysmaps_elements',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "selementid" },
        ]
      },
      {
        name: "sysmaps_elements_1",
        using: "BTREE",
        fields: [
          { name: "sysmapid" },
        ]
      },
      {
        name: "sysmaps_elements_2",
        using: "BTREE",
        fields: [
          { name: "iconid_off" },
        ]
      },
      {
        name: "sysmaps_elements_3",
        using: "BTREE",
        fields: [
          { name: "iconid_on" },
        ]
      },
      {
        name: "sysmaps_elements_4",
        using: "BTREE",
        fields: [
          { name: "iconid_disabled" },
        ]
      },
      {
        name: "sysmaps_elements_5",
        using: "BTREE",
        fields: [
          { name: "iconid_maintenance" },
        ]
      },
    ]
  });
};
