const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('token', {
    tokenid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    token: {
      type: DataTypes.STRING(128),
      allowNull: true,
      unique: "token_3"
    },
    lastaccess: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    expires_at: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    creator_userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'users',
        key: 'userid'
      }
    }
  }, {
    sequelize,
    tableName: 'token',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tokenid" },
        ]
      },
      {
        name: "token_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userid" },
          { name: "name" },
        ]
      },
      {
        name: "token_3",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "token" },
        ]
      },
      {
        name: "token_1",
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "token_4",
        using: "BTREE",
        fields: [
          { name: "creator_userid" },
        ]
      },
    ]
  });
};
