const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_condition', {
    lld_override_conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    lld_overrideid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'lld_override',
        key: 'lld_overrideid'
      }
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 8
    },
    macro: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'lld_override_condition',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_conditionid" },
        ]
      },
      {
        name: "lld_override_condition_1",
        using: "BTREE",
        fields: [
          { name: "lld_overrideid" },
        ]
      },
    ]
  });
};
