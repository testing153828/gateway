const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dashboard_usrgrp', {
    dashboard_usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    dashboardid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dashboard',
        key: 'dashboardid'
      }
    },
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    permission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    }
  }, {
    sequelize,
    tableName: 'dashboard_usrgrp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dashboard_usrgrpid" },
        ]
      },
      {
        name: "dashboard_usrgrp_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "dashboardid" },
          { name: "usrgrpid" },
        ]
      },
      {
        name: "c_dashboard_usrgrp_2",
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
    ]
  });
};
