const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('auditlog', {
    auditid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    action: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    resourcetype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    note: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    resourceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    resourcename: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'auditlog',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "auditid" },
        ]
      },
      {
        name: "auditlog_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
          { name: "clock" },
        ]
      },
      {
        name: "auditlog_2",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
      {
        name: "auditlog_3",
        using: "BTREE",
        fields: [
          { name: "resourcetype" },
          { name: "resourceid" },
        ]
      },
    ]
  });
};
