const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('item_rtdata', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    lastlogsize: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    mtime: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    error: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'item_rtdata',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
    ]
  });
};
