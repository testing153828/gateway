const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('widget_field', {
    widget_fieldid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    widgetid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'widget',
        key: 'widgetid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value_int: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value_str: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value_groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    },
    value_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    value_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    value_graphid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'graphs',
        key: 'graphid'
      }
    },
    value_sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    }
  }, {
    sequelize,
    tableName: 'widget_field',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "widget_fieldid" },
        ]
      },
      {
        name: "widget_field_1",
        using: "BTREE",
        fields: [
          { name: "widgetid" },
        ]
      },
      {
        name: "widget_field_2",
        using: "BTREE",
        fields: [
          { name: "value_groupid" },
        ]
      },
      {
        name: "widget_field_3",
        using: "BTREE",
        fields: [
          { name: "value_hostid" },
        ]
      },
      {
        name: "widget_field_4",
        using: "BTREE",
        fields: [
          { name: "value_itemid" },
        ]
      },
      {
        name: "widget_field_5",
        using: "BTREE",
        fields: [
          { name: "value_graphid" },
        ]
      },
      {
        name: "widget_field_6",
        using: "BTREE",
        fields: [
          { name: "value_sysmapid" },
        ]
      },
    ]
  });
};
