const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('event_recovery', {
    eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    r_eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    c_eventid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'events',
        key: 'eventid'
      }
    },
    correlationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'event_recovery',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "eventid" },
        ]
      },
      {
        name: "event_recovery_1",
        using: "BTREE",
        fields: [
          { name: "r_eventid" },
        ]
      },
      {
        name: "event_recovery_2",
        using: "BTREE",
        fields: [
          { name: "c_eventid" },
        ]
      },
    ]
  });
};
