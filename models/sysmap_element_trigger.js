const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmap_element_trigger', {
    selement_triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    selementid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps_elements',
        key: 'selementid'
      }
    },
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    }
  }, {
    sequelize,
    tableName: 'sysmap_element_trigger',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "selement_triggerid" },
        ]
      },
      {
        name: "sysmap_element_trigger_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "selementid" },
          { name: "triggerid" },
        ]
      },
      {
        name: "c_sysmap_element_trigger_2",
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
    ]
  });
};
