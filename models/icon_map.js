const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('icon_map', {
    iconmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "",
      unique: "icon_map_1"
    },
    default_iconid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'images',
        key: 'imageid'
      }
    }
  }, {
    sequelize,
    tableName: 'icon_map',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "iconmapid" },
        ]
      },
      {
        name: "icon_map_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "icon_map_2",
        using: "BTREE",
        fields: [
          { name: "default_iconid" },
        ]
      },
    ]
  });
};
