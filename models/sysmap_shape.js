const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmap_shape', {
    sysmap_shapeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    x: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    y: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 200
    },
    height: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 200
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    font: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 9
    },
    font_size: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 11
    },
    font_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "000000"
    },
    text_halign: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    text_valign: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    border_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    border_width: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    border_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "000000"
    },
    background_color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: ""
    },
    zindex: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'sysmap_shape',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sysmap_shapeid" },
        ]
      },
      {
        name: "sysmap_shape_1",
        using: "BTREE",
        fields: [
          { name: "sysmapid" },
        ]
      },
    ]
  });
};
