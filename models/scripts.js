const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('scripts', {
    scriptid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      unique: "scripts_3"
    },
    command: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    host_access: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    confirmation: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 5
    },
    execute_on: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    timeout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "30s"
    },
    scope: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    port: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    authtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    username: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    publickey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    privatekey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    menu_path: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'scripts',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "scriptid" },
        ]
      },
      {
        name: "scripts_3",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "scripts_1",
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
      {
        name: "scripts_2",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
