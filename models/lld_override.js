const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override', {
    lld_overrideid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    step: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    formula: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    stop: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'lld_override',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_overrideid" },
        ]
      },
      {
        name: "lld_override_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "name" },
        ]
      },
    ]
  });
};
