const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('media_type_message', {
    mediatype_messageid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'media_type',
        key: 'mediatypeid'
      }
    },
    eventsource: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    recovery: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    subject: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'media_type_message',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "mediatype_messageid" },
        ]
      },
      {
        name: "media_type_message_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
          { name: "eventsource" },
          { name: "recovery" },
        ]
      },
    ]
  });
};
