const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('graphs_items', {
    gitemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    graphid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'graphs',
        key: 'graphid'
      }
    },
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    drawtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sortorder: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "009600"
    },
    yaxisside: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    calc_fnc: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 2
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'graphs_items',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "gitemid" },
        ]
      },
      {
        name: "graphs_items_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
      {
        name: "graphs_items_2",
        using: "BTREE",
        fields: [
          { name: "graphid" },
        ]
      },
    ]
  });
};
