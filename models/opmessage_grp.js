const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opmessage_grp', {
    opmessage_grpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'usrgrp',
        key: 'usrgrpid'
      }
    }
  }, {
    sequelize,
    tableName: 'opmessage_grp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "opmessage_grpid" },
        ]
      },
      {
        name: "opmessage_grp_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
          { name: "usrgrpid" },
        ]
      },
      {
        name: "opmessage_grp_2",
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
    ]
  });
};
