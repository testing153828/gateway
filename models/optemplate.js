const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('optemplate', {
    optemplateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    }
  }, {
    sequelize,
    tableName: 'optemplate',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "optemplateid" },
        ]
      },
      {
        name: "optemplate_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
          { name: "templateid" },
        ]
      },
      {
        name: "optemplate_2",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
