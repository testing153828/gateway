const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('icon_mapping', {
    iconmappingid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    iconmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'icon_map',
        key: 'iconmapid'
      }
    },
    iconid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'images',
        key: 'imageid'
      }
    },
    inventory_link: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    expression: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    sortorder: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'icon_mapping',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "iconmappingid" },
        ]
      },
      {
        name: "icon_mapping_1",
        using: "BTREE",
        fields: [
          { name: "iconmapid" },
        ]
      },
      {
        name: "icon_mapping_2",
        using: "BTREE",
        fields: [
          { name: "iconid" },
        ]
      },
    ]
  });
};
