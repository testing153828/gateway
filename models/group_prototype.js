const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('group_prototype', {
    group_prototypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'group_prototype',
        key: 'group_prototypeid'
      }
    }
  }, {
    sequelize,
    tableName: 'group_prototype',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "group_prototypeid" },
        ]
      },
      {
        name: "group_prototype_1",
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
      {
        name: "c_group_prototype_2",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
      {
        name: "c_group_prototype_3",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
    ]
  });
};
