const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('autoreg_host', {
    autoreg_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    proxy_hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    host: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    listen_ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    listen_port: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    listen_dns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    host_metadata: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    tls_accepted: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    sequelize,
    tableName: 'autoreg_host',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "autoreg_hostid" },
        ]
      },
      {
        name: "autoreg_host_1",
        using: "BTREE",
        fields: [
          { name: "host" },
        ]
      },
      {
        name: "autoreg_host_2",
        using: "BTREE",
        fields: [
          { name: "proxy_hostid" },
        ]
      },
    ]
  });
};
