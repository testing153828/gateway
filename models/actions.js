const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('actions', {
    actionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      unique: "actions_2"
    },
    eventsource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    esc_period: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "1h"
    },
    formula: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    pause_suppressed: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    sequelize,
    tableName: 'actions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "actionid" },
        ]
      },
      {
        name: "actions_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "actions_1",
        using: "BTREE",
        fields: [
          { name: "eventsource" },
          { name: "status" },
        ]
      },
    ]
  });
};
