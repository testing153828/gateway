var DataTypes = require("sequelize").DataTypes;
var _acknowledges = require("./acknowledges");
var _actions = require("./actions");
var _alerts = require("./alerts");
var _auditlog = require("./auditlog");
var _auditlog_details = require("./auditlog_details");
var _autoreg_host = require("./autoreg_host");
var _conditions = require("./conditions");
var _config = require("./config");
var _config_autoreg_tls = require("./config_autoreg_tls");
var _corr_condition = require("./corr_condition");
var _corr_condition_group = require("./corr_condition_group");
var _corr_condition_tag = require("./corr_condition_tag");
var _corr_condition_tagpair = require("./corr_condition_tagpair");
var _corr_condition_tagvalue = require("./corr_condition_tagvalue");
var _corr_operation = require("./corr_operation");
var _correlation = require("./correlation");
var _dashboard = require("./dashboard");
var _dashboard_page = require("./dashboard_page");
var _dashboard_user = require("./dashboard_user");
var _dashboard_usrgrp = require("./dashboard_usrgrp");
var _dbversion = require("./dbversion");
var _dchecks = require("./dchecks");
var _dhosts = require("./dhosts");
var _drules = require("./drules");
var _dservices = require("./dservices");
var _escalations = require("./escalations");
var _event_recovery = require("./event_recovery");
var _event_suppress = require("./event_suppress");
var _event_tag = require("./event_tag");
var _events = require("./events");
var _expressions = require("./expressions");
var _functions = require("./functions");
var _globalmacro = require("./globalmacro");
var _globalvars = require("./globalvars");
var _graph_discovery = require("./graph_discovery");
var _graph_theme = require("./graph_theme");
var _graphs = require("./graphs");
var _graphs_items = require("./graphs_items");
var _group_discovery = require("./group_discovery");
var _group_prototype = require("./group_prototype");
var _history = require("./history");
var _history_log = require("./history_log");
var _history_str = require("./history_str");
var _history_text = require("./history_text");
var _history_uint = require("./history_uint");
var _host_discovery = require("./host_discovery");
var _host_inventory = require("./host_inventory");
var _host_tag = require("./host_tag");
var _hostmacro = require("./hostmacro");
var _hosts = require("./hosts");
var _hosts_groups = require("./hosts_groups");
var _hosts_templates = require("./hosts_templates");
var _housekeeper = require("./housekeeper");
var _hstgrp = require("./hstgrp");
var _httpstep = require("./httpstep");
var _httpstep_field = require("./httpstep_field");
var _httpstepitem = require("./httpstepitem");
var _httptest = require("./httptest");
var _httptest_field = require("./httptest_field");
var _httptest_tag = require("./httptest_tag");
var _httptestitem = require("./httptestitem");
var _icon_map = require("./icon_map");
var _icon_mapping = require("./icon_mapping");
var _ids = require("./ids");
var _images = require("./images");
var _interface = require("./interface");
var _interface_discovery = require("./interface_discovery");
var _interface_snmp = require("./interface_snmp");
var _item_condition = require("./item_condition");
var _item_discovery = require("./item_discovery");
var _item_parameter = require("./item_parameter");
var _item_preproc = require("./item_preproc");
var _item_rtdata = require("./item_rtdata");
var _item_tag = require("./item_tag");
var _items = require("./items");
var _lld_macro_path = require("./lld_macro_path");
var _lld_override = require("./lld_override");
var _lld_override_condition = require("./lld_override_condition");
var _lld_override_opdiscover = require("./lld_override_opdiscover");
var _lld_override_operation = require("./lld_override_operation");
var _lld_override_ophistory = require("./lld_override_ophistory");
var _lld_override_opinventory = require("./lld_override_opinventory");
var _lld_override_opperiod = require("./lld_override_opperiod");
var _lld_override_opseverity = require("./lld_override_opseverity");
var _lld_override_opstatus = require("./lld_override_opstatus");
var _lld_override_optag = require("./lld_override_optag");
var _lld_override_optemplate = require("./lld_override_optemplate");
var _lld_override_optrends = require("./lld_override_optrends");
var _maintenance_tag = require("./maintenance_tag");
var _maintenances = require("./maintenances");
var _maintenances_groups = require("./maintenances_groups");
var _maintenances_hosts = require("./maintenances_hosts");
var _maintenances_windows = require("./maintenances_windows");
var _media = require("./media");
var _media_type = require("./media_type");
var _media_type_message = require("./media_type_message");
var _media_type_param = require("./media_type_param");
var _module = require("./module");
var _opcommand = require("./opcommand");
var _opcommand_grp = require("./opcommand_grp");
var _opcommand_hst = require("./opcommand_hst");
var _opconditions = require("./opconditions");
var _operations = require("./operations");
var _opgroup = require("./opgroup");
var _opinventory = require("./opinventory");
var _opmessage = require("./opmessage");
var _opmessage_grp = require("./opmessage_grp");
var _opmessage_usr = require("./opmessage_usr");
var _optemplate = require("./optemplate");
var _problem = require("./problem");
var _problem_tag = require("./problem_tag");
var _profiles = require("./profiles");
var _proxy_autoreg_host = require("./proxy_autoreg_host");
var _proxy_dhistory = require("./proxy_dhistory");
var _proxy_history = require("./proxy_history");
var _regexps = require("./regexps");
var _report = require("./report");
var _report_param = require("./report_param");
var _report_user = require("./report_user");
var _report_usrgrp = require("./report_usrgrp");
var _rights = require("./rights");
var _role = require("./role");
var _role_rule = require("./role_rule");
var _script_param = require("./script_param");
var _scripts = require("./scripts");
var _service_alarms = require("./service_alarms");
var _services = require("./services");
var _services_links = require("./services_links");
var _services_times = require("./services_times");
var _sessions = require("./sessions");
var _sysmap_element_trigger = require("./sysmap_element_trigger");
var _sysmap_element_url = require("./sysmap_element_url");
var _sysmap_shape = require("./sysmap_shape");
var _sysmap_url = require("./sysmap_url");
var _sysmap_user = require("./sysmap_user");
var _sysmap_usrgrp = require("./sysmap_usrgrp");
var _sysmaps = require("./sysmaps");
var _sysmaps_element_tag = require("./sysmaps_element_tag");
var _sysmaps_elements = require("./sysmaps_elements");
var _sysmaps_link_triggers = require("./sysmaps_link_triggers");
var _sysmaps_links = require("./sysmaps_links");
var _tag_filter = require("./tag_filter");
var _task = require("./task");
var _task_acknowledge = require("./task_acknowledge");
var _task_check_now = require("./task_check_now");
var _task_close_problem = require("./task_close_problem");
var _task_data = require("./task_data");
var _task_remote_command = require("./task_remote_command");
var _task_remote_command_result = require("./task_remote_command_result");
var _task_result = require("./task_result");
var _timeperiods = require("./timeperiods");
var _token = require("./token");
var _trends = require("./trends");
var _trends_uint = require("./trends_uint");
var _trends_uint-test = require("./trends_uint-test");
var _trigger_depends = require("./trigger_depends");
var _trigger_discovery = require("./trigger_discovery");
var _trigger_queue = require("./trigger_queue");
var _trigger_tag = require("./trigger_tag");
var _triggers = require("./triggers");
var _users = require("./users");
var _users_groups = require("./users_groups");
var _usrgrp = require("./usrgrp");
var _valuemap = require("./valuemap");
var _valuemap_mapping = require("./valuemap_mapping");
var _widget = require("./widget");
var _widget_field = require("./widget_field");

function initModels(sequelize) {
  var acknowledges = _acknowledges(sequelize, DataTypes);
  var actions = _actions(sequelize, DataTypes);
  var alerts = _alerts(sequelize, DataTypes);
  var auditlog = _auditlog(sequelize, DataTypes);
  var auditlog_details = _auditlog_details(sequelize, DataTypes);
  var autoreg_host = _autoreg_host(sequelize, DataTypes);
  var conditions = _conditions(sequelize, DataTypes);
  var config = _config(sequelize, DataTypes);
  var config_autoreg_tls = _config_autoreg_tls(sequelize, DataTypes);
  var corr_condition = _corr_condition(sequelize, DataTypes);
  var corr_condition_group = _corr_condition_group(sequelize, DataTypes);
  var corr_condition_tag = _corr_condition_tag(sequelize, DataTypes);
  var corr_condition_tagpair = _corr_condition_tagpair(sequelize, DataTypes);
  var corr_condition_tagvalue = _corr_condition_tagvalue(sequelize, DataTypes);
  var corr_operation = _corr_operation(sequelize, DataTypes);
  var correlation = _correlation(sequelize, DataTypes);
  var dashboard = _dashboard(sequelize, DataTypes);
  var dashboard_page = _dashboard_page(sequelize, DataTypes);
  var dashboard_user = _dashboard_user(sequelize, DataTypes);
  var dashboard_usrgrp = _dashboard_usrgrp(sequelize, DataTypes);
  var dbversion = _dbversion(sequelize, DataTypes);
  var dchecks = _dchecks(sequelize, DataTypes);
  var dhosts = _dhosts(sequelize, DataTypes);
  var drules = _drules(sequelize, DataTypes);
  var dservices = _dservices(sequelize, DataTypes);
  var escalations = _escalations(sequelize, DataTypes);
  var event_recovery = _event_recovery(sequelize, DataTypes);
  var event_suppress = _event_suppress(sequelize, DataTypes);
  var event_tag = _event_tag(sequelize, DataTypes);
  var events = _events(sequelize, DataTypes);
  var expressions = _expressions(sequelize, DataTypes);
  var functions = _functions(sequelize, DataTypes);
  var globalmacro = _globalmacro(sequelize, DataTypes);
  var globalvars = _globalvars(sequelize, DataTypes);
  var graph_discovery = _graph_discovery(sequelize, DataTypes);
  var graph_theme = _graph_theme(sequelize, DataTypes);
  var graphs = _graphs(sequelize, DataTypes);
  var graphs_items = _graphs_items(sequelize, DataTypes);
  var group_discovery = _group_discovery(sequelize, DataTypes);
  var group_prototype = _group_prototype(sequelize, DataTypes);
  var history = _history(sequelize, DataTypes);
  var history_log = _history_log(sequelize, DataTypes);
  var history_str = _history_str(sequelize, DataTypes);
  var history_text = _history_text(sequelize, DataTypes);
  var history_uint = _history_uint(sequelize, DataTypes);
  var host_discovery = _host_discovery(sequelize, DataTypes);
  var host_inventory = _host_inventory(sequelize, DataTypes);
  var host_tag = _host_tag(sequelize, DataTypes);
  var hostmacro = _hostmacro(sequelize, DataTypes);
  var hosts = _hosts(sequelize, DataTypes);
  var hosts_groups = _hosts_groups(sequelize, DataTypes);
  var hosts_templates = _hosts_templates(sequelize, DataTypes);
  var housekeeper = _housekeeper(sequelize, DataTypes);
  var hstgrp = _hstgrp(sequelize, DataTypes);
  var httpstep = _httpstep(sequelize, DataTypes);
  var httpstep_field = _httpstep_field(sequelize, DataTypes);
  var httpstepitem = _httpstepitem(sequelize, DataTypes);
  var httptest = _httptest(sequelize, DataTypes);
  var httptest_field = _httptest_field(sequelize, DataTypes);
  var httptest_tag = _httptest_tag(sequelize, DataTypes);
  var httptestitem = _httptestitem(sequelize, DataTypes);
  var icon_map = _icon_map(sequelize, DataTypes);
  var icon_mapping = _icon_mapping(sequelize, DataTypes);
  var ids = _ids(sequelize, DataTypes);
  var images = _images(sequelize, DataTypes);
  var interface = _interface(sequelize, DataTypes);
  var interface_discovery = _interface_discovery(sequelize, DataTypes);
  var interface_snmp = _interface_snmp(sequelize, DataTypes);
  var item_condition = _item_condition(sequelize, DataTypes);
  var item_discovery = _item_discovery(sequelize, DataTypes);
  var item_parameter = _item_parameter(sequelize, DataTypes);
  var item_preproc = _item_preproc(sequelize, DataTypes);
  var item_rtdata = _item_rtdata(sequelize, DataTypes);
  var item_tag = _item_tag(sequelize, DataTypes);
  var items = _items(sequelize, DataTypes);
  var lld_macro_path = _lld_macro_path(sequelize, DataTypes);
  var lld_override = _lld_override(sequelize, DataTypes);
  var lld_override_condition = _lld_override_condition(sequelize, DataTypes);
  var lld_override_opdiscover = _lld_override_opdiscover(sequelize, DataTypes);
  var lld_override_operation = _lld_override_operation(sequelize, DataTypes);
  var lld_override_ophistory = _lld_override_ophistory(sequelize, DataTypes);
  var lld_override_opinventory = _lld_override_opinventory(sequelize, DataTypes);
  var lld_override_opperiod = _lld_override_opperiod(sequelize, DataTypes);
  var lld_override_opseverity = _lld_override_opseverity(sequelize, DataTypes);
  var lld_override_opstatus = _lld_override_opstatus(sequelize, DataTypes);
  var lld_override_optag = _lld_override_optag(sequelize, DataTypes);
  var lld_override_optemplate = _lld_override_optemplate(sequelize, DataTypes);
  var lld_override_optrends = _lld_override_optrends(sequelize, DataTypes);
  var maintenance_tag = _maintenance_tag(sequelize, DataTypes);
  var maintenances = _maintenances(sequelize, DataTypes);
  var maintenances_groups = _maintenances_groups(sequelize, DataTypes);
  var maintenances_hosts = _maintenances_hosts(sequelize, DataTypes);
  var maintenances_windows = _maintenances_windows(sequelize, DataTypes);
  var media = _media(sequelize, DataTypes);
  var media_type = _media_type(sequelize, DataTypes);
  var media_type_message = _media_type_message(sequelize, DataTypes);
  var media_type_param = _media_type_param(sequelize, DataTypes);
  var module = _module(sequelize, DataTypes);
  var opcommand = _opcommand(sequelize, DataTypes);
  var opcommand_grp = _opcommand_grp(sequelize, DataTypes);
  var opcommand_hst = _opcommand_hst(sequelize, DataTypes);
  var opconditions = _opconditions(sequelize, DataTypes);
  var operations = _operations(sequelize, DataTypes);
  var opgroup = _opgroup(sequelize, DataTypes);
  var opinventory = _opinventory(sequelize, DataTypes);
  var opmessage = _opmessage(sequelize, DataTypes);
  var opmessage_grp = _opmessage_grp(sequelize, DataTypes);
  var opmessage_usr = _opmessage_usr(sequelize, DataTypes);
  var optemplate = _optemplate(sequelize, DataTypes);
  var problem = _problem(sequelize, DataTypes);
  var problem_tag = _problem_tag(sequelize, DataTypes);
  var profiles = _profiles(sequelize, DataTypes);
  var proxy_autoreg_host = _proxy_autoreg_host(sequelize, DataTypes);
  var proxy_dhistory = _proxy_dhistory(sequelize, DataTypes);
  var proxy_history = _proxy_history(sequelize, DataTypes);
  var regexps = _regexps(sequelize, DataTypes);
  var report = _report(sequelize, DataTypes);
  var report_param = _report_param(sequelize, DataTypes);
  var report_user = _report_user(sequelize, DataTypes);
  var report_usrgrp = _report_usrgrp(sequelize, DataTypes);
  var rights = _rights(sequelize, DataTypes);
  var role = _role(sequelize, DataTypes);
  var role_rule = _role_rule(sequelize, DataTypes);
  var script_param = _script_param(sequelize, DataTypes);
  var scripts = _scripts(sequelize, DataTypes);
  var service_alarms = _service_alarms(sequelize, DataTypes);
  var services = _services(sequelize, DataTypes);
  var services_links = _services_links(sequelize, DataTypes);
  var services_times = _services_times(sequelize, DataTypes);
  var sessions = _sessions(sequelize, DataTypes);
  var sysmap_element_trigger = _sysmap_element_trigger(sequelize, DataTypes);
  var sysmap_element_url = _sysmap_element_url(sequelize, DataTypes);
  var sysmap_shape = _sysmap_shape(sequelize, DataTypes);
  var sysmap_url = _sysmap_url(sequelize, DataTypes);
  var sysmap_user = _sysmap_user(sequelize, DataTypes);
  var sysmap_usrgrp = _sysmap_usrgrp(sequelize, DataTypes);
  var sysmaps = _sysmaps(sequelize, DataTypes);
  var sysmaps_element_tag = _sysmaps_element_tag(sequelize, DataTypes);
  var sysmaps_elements = _sysmaps_elements(sequelize, DataTypes);
  var sysmaps_link_triggers = _sysmaps_link_triggers(sequelize, DataTypes);
  var sysmaps_links = _sysmaps_links(sequelize, DataTypes);
  var tag_filter = _tag_filter(sequelize, DataTypes);
  var task = _task(sequelize, DataTypes);
  var task_acknowledge = _task_acknowledge(sequelize, DataTypes);
  var task_check_now = _task_check_now(sequelize, DataTypes);
  var task_close_problem = _task_close_problem(sequelize, DataTypes);
  var task_data = _task_data(sequelize, DataTypes);
  var task_remote_command = _task_remote_command(sequelize, DataTypes);
  var task_remote_command_result = _task_remote_command_result(sequelize, DataTypes);
  var task_result = _task_result(sequelize, DataTypes);
  var timeperiods = _timeperiods(sequelize, DataTypes);
  var token = _token(sequelize, DataTypes);
  var trends = _trends(sequelize, DataTypes);
  var trends_uint = _trends_uint(sequelize, DataTypes);
  var trends_uint-test = _trends_uint-test(sequelize, DataTypes);
  var trigger_depends = _trigger_depends(sequelize, DataTypes);
  var trigger_discovery = _trigger_discovery(sequelize, DataTypes);
  var trigger_queue = _trigger_queue(sequelize, DataTypes);
  var trigger_tag = _trigger_tag(sequelize, DataTypes);
  var triggers = _triggers(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);
  var users_groups = _users_groups(sequelize, DataTypes);
  var usrgrp = _usrgrp(sequelize, DataTypes);
  var valuemap = _valuemap(sequelize, DataTypes);
  var valuemap_mapping = _valuemap_mapping(sequelize, DataTypes);
  var widget = _widget(sequelize, DataTypes);
  var widget_field = _widget_field(sequelize, DataTypes);

  alerts.belongsTo(acknowledges, { as: "acknowledge", foreignKey: "acknowledgeid"});
  acknowledges.hasMany(alerts, { as: "alerts", foreignKey: "acknowledgeid"});
  alerts.belongsTo(actions, { as: "action", foreignKey: "actionid"});
  actions.hasMany(alerts, { as: "alerts", foreignKey: "actionid"});
  conditions.belongsTo(actions, { as: "action", foreignKey: "actionid"});
  actions.hasMany(conditions, { as: "conditions", foreignKey: "actionid"});
  operations.belongsTo(actions, { as: "action", foreignKey: "actionid"});
  actions.hasMany(operations, { as: "operations", foreignKey: "actionid"});
  auditlog_details.belongsTo(auditlog, { as: "audit", foreignKey: "auditid"});
  auditlog.hasMany(auditlog_details, { as: "auditlog_details", foreignKey: "auditid"});
  corr_condition_group.belongsTo(corr_condition, { as: "corr_condition", foreignKey: "corr_conditionid"});
  corr_condition.hasOne(corr_condition_group, { as: "corr_condition_group", foreignKey: "corr_conditionid"});
  corr_condition_tag.belongsTo(corr_condition, { as: "corr_condition", foreignKey: "corr_conditionid"});
  corr_condition.hasOne(corr_condition_tag, { as: "corr_condition_tag", foreignKey: "corr_conditionid"});
  corr_condition_tagpair.belongsTo(corr_condition, { as: "corr_condition", foreignKey: "corr_conditionid"});
  corr_condition.hasOne(corr_condition_tagpair, { as: "corr_condition_tagpair", foreignKey: "corr_conditionid"});
  corr_condition_tagvalue.belongsTo(corr_condition, { as: "corr_condition", foreignKey: "corr_conditionid"});
  corr_condition.hasOne(corr_condition_tagvalue, { as: "corr_condition_tagvalue", foreignKey: "corr_conditionid"});
  corr_condition.belongsTo(correlation, { as: "correlation", foreignKey: "correlationid"});
  correlation.hasMany(corr_condition, { as: "corr_conditions", foreignKey: "correlationid"});
  corr_operation.belongsTo(correlation, { as: "correlation", foreignKey: "correlationid"});
  correlation.hasMany(corr_operation, { as: "corr_operations", foreignKey: "correlationid"});
  dashboard_page.belongsTo(dashboard, { as: "dashboard", foreignKey: "dashboardid"});
  dashboard.hasMany(dashboard_page, { as: "dashboard_pages", foreignKey: "dashboardid"});
  dashboard_user.belongsTo(dashboard, { as: "dashboard", foreignKey: "dashboardid"});
  dashboard.hasMany(dashboard_user, { as: "dashboard_users", foreignKey: "dashboardid"});
  dashboard_usrgrp.belongsTo(dashboard, { as: "dashboard", foreignKey: "dashboardid"});
  dashboard.hasMany(dashboard_usrgrp, { as: "dashboard_usrgrps", foreignKey: "dashboardid"});
  report.belongsTo(dashboard, { as: "dashboard", foreignKey: "dashboardid"});
  dashboard.hasMany(report, { as: "reports", foreignKey: "dashboardid"});
  widget.belongsTo(dashboard_page, { as: "dashboard_page", foreignKey: "dashboard_pageid"});
  dashboard_page.hasMany(widget, { as: "widgets", foreignKey: "dashboard_pageid"});
  dservices.belongsTo(dchecks, { as: "dcheck", foreignKey: "dcheckid"});
  dchecks.hasMany(dservices, { as: "dservices", foreignKey: "dcheckid"});
  dservices.belongsTo(dhosts, { as: "dhost", foreignKey: "dhostid"});
  dhosts.hasMany(dservices, { as: "dservices", foreignKey: "dhostid"});
  dchecks.belongsTo(drules, { as: "drule", foreignKey: "druleid"});
  drules.hasMany(dchecks, { as: "dchecks", foreignKey: "druleid"});
  dhosts.belongsTo(drules, { as: "drule", foreignKey: "druleid"});
  drules.hasMany(dhosts, { as: "dhosts", foreignKey: "druleid"});
  acknowledges.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasMany(acknowledges, { as: "acknowledges", foreignKey: "eventid"});
  alerts.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasMany(alerts, { as: "alerts", foreignKey: "eventid"});
  alerts.belongsTo(events, { as: "p_event", foreignKey: "p_eventid"});
  events.hasMany(alerts, { as: "p_event_alerts", foreignKey: "p_eventid"});
  event_recovery.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasOne(event_recovery, { as: "event_recovery", foreignKey: "eventid"});
  event_recovery.belongsTo(events, { as: "r_event", foreignKey: "r_eventid"});
  events.hasMany(event_recovery, { as: "r_event_event_recoveries", foreignKey: "r_eventid"});
  event_recovery.belongsTo(events, { as: "c_event", foreignKey: "c_eventid"});
  events.hasMany(event_recovery, { as: "c_event_event_recoveries", foreignKey: "c_eventid"});
  event_suppress.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasMany(event_suppress, { as: "event_suppresses", foreignKey: "eventid"});
  event_tag.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasMany(event_tag, { as: "event_tags", foreignKey: "eventid"});
  problem.belongsTo(events, { as: "event", foreignKey: "eventid"});
  events.hasOne(problem, { as: "problem", foreignKey: "eventid"});
  problem.belongsTo(events, { as: "r_event", foreignKey: "r_eventid"});
  events.hasMany(problem, { as: "r_event_problems", foreignKey: "r_eventid"});
  graph_discovery.belongsTo(graphs, { as: "graph", foreignKey: "graphid"});
  graphs.hasOne(graph_discovery, { as: "graph_discovery", foreignKey: "graphid"});
  graph_discovery.belongsTo(graphs, { as: "parent_graph", foreignKey: "parent_graphid"});
  graphs.hasMany(graph_discovery, { as: "parent_graph_graph_discoveries", foreignKey: "parent_graphid"});
  graphs.belongsTo(graphs, { as: "template", foreignKey: "templateid"});
  graphs.hasMany(graphs, { as: "graphs", foreignKey: "templateid"});
  graphs_items.belongsTo(graphs, { as: "graph", foreignKey: "graphid"});
  graphs.hasMany(graphs_items, { as: "graphs_items", foreignKey: "graphid"});
  widget_field.belongsTo(graphs, { as: "value_graph", foreignKey: "value_graphid"});
  graphs.hasMany(widget_field, { as: "widget_fields", foreignKey: "value_graphid"});
  group_discovery.belongsTo(group_prototype, { as: "parent_group_prototype", foreignKey: "parent_group_prototypeid"});
  group_prototype.hasMany(group_discovery, { as: "group_discoveries", foreignKey: "parent_group_prototypeid"});
  group_prototype.belongsTo(group_prototype, { as: "template", foreignKey: "templateid"});
  group_prototype.hasMany(group_prototype, { as: "group_prototypes", foreignKey: "templateid"});
  autoreg_host.belongsTo(hosts, { as: "proxy_host", foreignKey: "proxy_hostid"});
  hosts.hasMany(autoreg_host, { as: "autoreg_hosts", foreignKey: "proxy_hostid"});
  dashboard.belongsTo(hosts, { as: "template", foreignKey: "templateid"});
  hosts.hasMany(dashboard, { as: "dashboards", foreignKey: "templateid"});
  drules.belongsTo(hosts, { as: "proxy_host", foreignKey: "proxy_hostid"});
  hosts.hasMany(drules, { as: "drules", foreignKey: "proxy_hostid"});
  group_prototype.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(group_prototype, { as: "group_prototypes", foreignKey: "hostid"});
  host_discovery.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasOne(host_discovery, { as: "host_discovery", foreignKey: "hostid"});
  host_discovery.belongsTo(hosts, { as: "parent_host", foreignKey: "parent_hostid"});
  hosts.hasMany(host_discovery, { as: "parent_host_host_discoveries", foreignKey: "parent_hostid"});
  host_inventory.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasOne(host_inventory, { as: "host_inventory", foreignKey: "hostid"});
  host_tag.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(host_tag, { as: "host_tags", foreignKey: "hostid"});
  hostmacro.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(hostmacro, { as: "hostmacros", foreignKey: "hostid"});
  hosts.belongsTo(hosts, { as: "proxy_host", foreignKey: "proxy_hostid"});
  hosts.hasMany(hosts, { as: "hosts", foreignKey: "proxy_hostid"});
  hosts.belongsTo(hosts, { as: "template", foreignKey: "templateid"});
  hosts.hasMany(hosts, { as: "template_hosts", foreignKey: "templateid"});
  hosts_groups.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(hosts_groups, { as: "hosts_groups", foreignKey: "hostid"});
  hosts_templates.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(hosts_templates, { as: "hosts_templates", foreignKey: "hostid"});
  hosts_templates.belongsTo(hosts, { as: "template", foreignKey: "templateid"});
  hosts.hasMany(hosts_templates, { as: "template_hosts_templates", foreignKey: "templateid"});
  httptest.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(httptest, { as: "httptests", foreignKey: "hostid"});
  interface.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(interface, { as: "interfaces", foreignKey: "hostid"});
  items.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(items, { as: "items", foreignKey: "hostid"});
  lld_override_optemplate.belongsTo(hosts, { as: "template", foreignKey: "templateid"});
  hosts.hasMany(lld_override_optemplate, { as: "lld_override_optemplates", foreignKey: "templateid"});
  maintenances_hosts.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(maintenances_hosts, { as: "maintenances_hosts", foreignKey: "hostid"});
  opcommand_hst.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(opcommand_hst, { as: "opcommand_hsts", foreignKey: "hostid"});
  optemplate.belongsTo(hosts, { as: "template", foreignKey: "templateid"});
  hosts.hasMany(optemplate, { as: "optemplates", foreignKey: "templateid"});
  task.belongsTo(hosts, { as: "proxy_host", foreignKey: "proxy_hostid"});
  hosts.hasMany(task, { as: "tasks", foreignKey: "proxy_hostid"});
  valuemap.belongsTo(hosts, { as: "host", foreignKey: "hostid"});
  hosts.hasMany(valuemap, { as: "valuemaps", foreignKey: "hostid"});
  widget_field.belongsTo(hosts, { as: "value_host", foreignKey: "value_hostid"});
  hosts.hasMany(widget_field, { as: "widget_fields", foreignKey: "value_hostid"});
  config.belongsTo(hstgrp, { as: "discovery_group", foreignKey: "discovery_groupid"});
  hstgrp.hasMany(config, { as: "configs", foreignKey: "discovery_groupid"});
  corr_condition_group.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(corr_condition_group, { as: "corr_condition_groups", foreignKey: "groupid"});
  group_discovery.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasOne(group_discovery, { as: "group_discovery", foreignKey: "groupid"});
  group_prototype.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(group_prototype, { as: "group_prototypes", foreignKey: "groupid"});
  hosts_groups.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(hosts_groups, { as: "hosts_groups", foreignKey: "groupid"});
  maintenances_groups.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(maintenances_groups, { as: "maintenances_groups", foreignKey: "groupid"});
  opcommand_grp.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(opcommand_grp, { as: "opcommand_grps", foreignKey: "groupid"});
  opgroup.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(opgroup, { as: "opgroups", foreignKey: "groupid"});
  rights.belongsTo(hstgrp, { as: "id_hstgrp", foreignKey: "id"});
  hstgrp.hasMany(rights, { as: "rights", foreignKey: "id"});
  scripts.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(scripts, { as: "scripts", foreignKey: "groupid"});
  tag_filter.belongsTo(hstgrp, { as: "group", foreignKey: "groupid"});
  hstgrp.hasMany(tag_filter, { as: "tag_filters", foreignKey: "groupid"});
  widget_field.belongsTo(hstgrp, { as: "value_group", foreignKey: "value_groupid"});
  hstgrp.hasMany(widget_field, { as: "widget_fields", foreignKey: "value_groupid"});
  httpstep_field.belongsTo(httpstep, { as: "httpstep", foreignKey: "httpstepid"});
  httpstep.hasMany(httpstep_field, { as: "httpstep_fields", foreignKey: "httpstepid"});
  httpstepitem.belongsTo(httpstep, { as: "httpstep", foreignKey: "httpstepid"});
  httpstep.hasMany(httpstepitem, { as: "httpstepitems", foreignKey: "httpstepid"});
  httpstep.belongsTo(httptest, { as: "httptest", foreignKey: "httptestid"});
  httptest.hasMany(httpstep, { as: "httpsteps", foreignKey: "httptestid"});
  httptest.belongsTo(httptest, { as: "template", foreignKey: "templateid"});
  httptest.hasMany(httptest, { as: "httptests", foreignKey: "templateid"});
  httptest_field.belongsTo(httptest, { as: "httptest", foreignKey: "httptestid"});
  httptest.hasMany(httptest_field, { as: "httptest_fields", foreignKey: "httptestid"});
  httptest_tag.belongsTo(httptest, { as: "httptest", foreignKey: "httptestid"});
  httptest.hasMany(httptest_tag, { as: "httptest_tags", foreignKey: "httptestid"});
  httptestitem.belongsTo(httptest, { as: "httptest", foreignKey: "httptestid"});
  httptest.hasMany(httptestitem, { as: "httptestitems", foreignKey: "httptestid"});
  icon_mapping.belongsTo(icon_map, { as: "iconmap", foreignKey: "iconmapid"});
  icon_map.hasMany(icon_mapping, { as: "icon_mappings", foreignKey: "iconmapid"});
  sysmaps.belongsTo(icon_map, { as: "iconmap", foreignKey: "iconmapid"});
  icon_map.hasMany(sysmaps, { as: "sysmaps", foreignKey: "iconmapid"});
  icon_map.belongsTo(images, { as: "default_icon", foreignKey: "default_iconid"});
  images.hasMany(icon_map, { as: "icon_maps", foreignKey: "default_iconid"});
  icon_mapping.belongsTo(images, { as: "icon", foreignKey: "iconid"});
  images.hasMany(icon_mapping, { as: "icon_mappings", foreignKey: "iconid"});
  sysmaps.belongsTo(images, { as: "background", foreignKey: "backgroundid"});
  images.hasMany(sysmaps, { as: "sysmaps", foreignKey: "backgroundid"});
  sysmaps_elements.belongsTo(images, { as: "iconid_off_image", foreignKey: "iconid_off"});
  images.hasMany(sysmaps_elements, { as: "sysmaps_elements", foreignKey: "iconid_off"});
  sysmaps_elements.belongsTo(images, { as: "iconid_on_image", foreignKey: "iconid_on"});
  images.hasMany(sysmaps_elements, { as: "iconid_on_sysmaps_elements", foreignKey: "iconid_on"});
  sysmaps_elements.belongsTo(images, { as: "iconid_disabled_image", foreignKey: "iconid_disabled"});
  images.hasMany(sysmaps_elements, { as: "iconid_disabled_sysmaps_elements", foreignKey: "iconid_disabled"});
  sysmaps_elements.belongsTo(images, { as: "iconid_maintenance_image", foreignKey: "iconid_maintenance"});
  images.hasMany(sysmaps_elements, { as: "iconid_maintenance_sysmaps_elements", foreignKey: "iconid_maintenance"});
  interface_discovery.belongsTo(interface, { as: "interface", foreignKey: "interfaceid"});
  interface.hasOne(interface_discovery, { as: "interface_discovery", foreignKey: "interfaceid"});
  interface_discovery.belongsTo(interface, { as: "parent_interface", foreignKey: "parent_interfaceid"});
  interface.hasMany(interface_discovery, { as: "parent_interface_interface_discoveries", foreignKey: "parent_interfaceid"});
  interface_snmp.belongsTo(interface, { as: "interface", foreignKey: "interfaceid"});
  interface.hasOne(interface_snmp, { as: "interface_snmp", foreignKey: "interfaceid"});
  items.belongsTo(interface, { as: "interface", foreignKey: "interfaceid"});
  interface.hasMany(items, { as: "items", foreignKey: "interfaceid"});
  functions.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(functions, { as: "functions", foreignKey: "itemid"});
  graphs.belongsTo(items, { as: "ymin_item", foreignKey: "ymin_itemid"});
  items.hasMany(graphs, { as: "graphs", foreignKey: "ymin_itemid"});
  graphs.belongsTo(items, { as: "ymax_item", foreignKey: "ymax_itemid"});
  items.hasMany(graphs, { as: "ymax_item_graphs", foreignKey: "ymax_itemid"});
  graphs_items.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(graphs_items, { as: "graphs_items", foreignKey: "itemid"});
  host_discovery.belongsTo(items, { as: "parent_item", foreignKey: "parent_itemid"});
  items.hasMany(host_discovery, { as: "host_discoveries", foreignKey: "parent_itemid"});
  httpstepitem.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(httpstepitem, { as: "httpstepitems", foreignKey: "itemid"});
  httptestitem.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(httptestitem, { as: "httptestitems", foreignKey: "itemid"});
  item_condition.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(item_condition, { as: "item_conditions", foreignKey: "itemid"});
  item_discovery.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(item_discovery, { as: "item_discoveries", foreignKey: "itemid"});
  item_discovery.belongsTo(items, { as: "parent_item", foreignKey: "parent_itemid"});
  items.hasMany(item_discovery, { as: "parent_item_item_discoveries", foreignKey: "parent_itemid"});
  item_parameter.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(item_parameter, { as: "item_parameters", foreignKey: "itemid"});
  item_preproc.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(item_preproc, { as: "item_preprocs", foreignKey: "itemid"});
  item_rtdata.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasOne(item_rtdata, { as: "item_rtdatum", foreignKey: "itemid"});
  item_tag.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(item_tag, { as: "item_tags", foreignKey: "itemid"});
  items.belongsTo(items, { as: "template", foreignKey: "templateid"});
  items.hasMany(items, { as: "items", foreignKey: "templateid"});
  items.belongsTo(items, { as: "master_item", foreignKey: "master_itemid"});
  items.hasMany(items, { as: "master_item_items", foreignKey: "master_itemid"});
  lld_macro_path.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(lld_macro_path, { as: "lld_macro_paths", foreignKey: "itemid"});
  lld_override.belongsTo(items, { as: "item", foreignKey: "itemid"});
  items.hasMany(lld_override, { as: "lld_overrides", foreignKey: "itemid"});
  widget_field.belongsTo(items, { as: "value_item", foreignKey: "value_itemid"});
  items.hasMany(widget_field, { as: "widget_fields", foreignKey: "value_itemid"});
  lld_override_condition.belongsTo(lld_override, { as: "lld_override", foreignKey: "lld_overrideid"});
  lld_override.hasMany(lld_override_condition, { as: "lld_override_conditions", foreignKey: "lld_overrideid"});
  lld_override_operation.belongsTo(lld_override, { as: "lld_override", foreignKey: "lld_overrideid"});
  lld_override.hasMany(lld_override_operation, { as: "lld_override_operations", foreignKey: "lld_overrideid"});
  lld_override_opdiscover.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_opdiscover, { as: "lld_override_opdiscover", foreignKey: "lld_override_operationid"});
  lld_override_ophistory.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_ophistory, { as: "lld_override_ophistory", foreignKey: "lld_override_operationid"});
  lld_override_opinventory.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_opinventory, { as: "lld_override_opinventory", foreignKey: "lld_override_operationid"});
  lld_override_opperiod.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_opperiod, { as: "lld_override_opperiod", foreignKey: "lld_override_operationid"});
  lld_override_opseverity.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_opseverity, { as: "lld_override_opseverity", foreignKey: "lld_override_operationid"});
  lld_override_opstatus.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_opstatus, { as: "lld_override_opstatus", foreignKey: "lld_override_operationid"});
  lld_override_optag.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasMany(lld_override_optag, { as: "lld_override_optags", foreignKey: "lld_override_operationid"});
  lld_override_optemplate.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasMany(lld_override_optemplate, { as: "lld_override_optemplates", foreignKey: "lld_override_operationid"});
  lld_override_optrends.belongsTo(lld_override_operation, { as: "lld_override_operation", foreignKey: "lld_override_operationid"});
  lld_override_operation.hasOne(lld_override_optrends, { as: "lld_override_optrend", foreignKey: "lld_override_operationid"});
  event_suppress.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(event_suppress, { as: "event_suppresses", foreignKey: "maintenanceid"});
  hosts.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(hosts, { as: "hosts", foreignKey: "maintenanceid"});
  maintenance_tag.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(maintenance_tag, { as: "maintenance_tags", foreignKey: "maintenanceid"});
  maintenances_groups.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(maintenances_groups, { as: "maintenances_groups", foreignKey: "maintenanceid"});
  maintenances_hosts.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(maintenances_hosts, { as: "maintenances_hosts", foreignKey: "maintenanceid"});
  maintenances_windows.belongsTo(maintenances, { as: "maintenance", foreignKey: "maintenanceid"});
  maintenances.hasMany(maintenances_windows, { as: "maintenances_windows", foreignKey: "maintenanceid"});
  alerts.belongsTo(media_type, { as: "mediatype", foreignKey: "mediatypeid"});
  media_type.hasMany(alerts, { as: "alerts", foreignKey: "mediatypeid"});
  media.belongsTo(media_type, { as: "mediatype", foreignKey: "mediatypeid"});
  media_type.hasMany(media, { as: "media", foreignKey: "mediatypeid"});
  media_type_message.belongsTo(media_type, { as: "mediatype", foreignKey: "mediatypeid"});
  media_type.hasMany(media_type_message, { as: "media_type_messages", foreignKey: "mediatypeid"});
  media_type_param.belongsTo(media_type, { as: "mediatype", foreignKey: "mediatypeid"});
  media_type.hasMany(media_type_param, { as: "media_type_params", foreignKey: "mediatypeid"});
  opmessage.belongsTo(media_type, { as: "mediatype", foreignKey: "mediatypeid"});
  media_type.hasMany(opmessage, { as: "opmessages", foreignKey: "mediatypeid"});
  role_rule.belongsTo(module, { as: "value_module", foreignKey: "value_moduleid"});
  module.hasMany(role_rule, { as: "role_rules", foreignKey: "value_moduleid"});
  opcommand.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasOne(opcommand, { as: "opcommand", foreignKey: "operationid"});
  opcommand_grp.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opcommand_grp, { as: "opcommand_grps", foreignKey: "operationid"});
  opcommand_hst.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opcommand_hst, { as: "opcommand_hsts", foreignKey: "operationid"});
  opconditions.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opconditions, { as: "opconditions", foreignKey: "operationid"});
  opgroup.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opgroup, { as: "opgroups", foreignKey: "operationid"});
  opinventory.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasOne(opinventory, { as: "opinventory", foreignKey: "operationid"});
  opmessage.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasOne(opmessage, { as: "opmessage", foreignKey: "operationid"});
  opmessage_grp.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opmessage_grp, { as: "opmessage_grps", foreignKey: "operationid"});
  opmessage_usr.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(opmessage_usr, { as: "opmessage_usrs", foreignKey: "operationid"});
  optemplate.belongsTo(operations, { as: "operation", foreignKey: "operationid"});
  operations.hasMany(optemplate, { as: "optemplates", foreignKey: "operationid"});
  problem_tag.belongsTo(problem, { as: "event", foreignKey: "eventid"});
  problem.hasMany(problem_tag, { as: "problem_tags", foreignKey: "eventid"});
  expressions.belongsTo(regexps, { as: "regexp", foreignKey: "regexpid"});
  regexps.hasMany(expressions, { as: "expressions", foreignKey: "regexpid"});
  report_param.belongsTo(report, { as: "report", foreignKey: "reportid"});
  report.hasMany(report_param, { as: "report_params", foreignKey: "reportid"});
  report_user.belongsTo(report, { as: "report", foreignKey: "reportid"});
  report.hasMany(report_user, { as: "report_users", foreignKey: "reportid"});
  report_usrgrp.belongsTo(report, { as: "report", foreignKey: "reportid"});
  report.hasMany(report_usrgrp, { as: "report_usrgrps", foreignKey: "reportid"});
  role_rule.belongsTo(role, { as: "role", foreignKey: "roleid"});
  role.hasMany(role_rule, { as: "role_rules", foreignKey: "roleid"});
  users.belongsTo(role, { as: "role", foreignKey: "roleid"});
  role.hasMany(users, { as: "users", foreignKey: "roleid"});
  opcommand.belongsTo(scripts, { as: "script", foreignKey: "scriptid"});
  scripts.hasMany(opcommand, { as: "opcommands", foreignKey: "scriptid"});
  script_param.belongsTo(scripts, { as: "script", foreignKey: "scriptid"});
  scripts.hasMany(script_param, { as: "script_params", foreignKey: "scriptid"});
  service_alarms.belongsTo(services, { as: "service", foreignKey: "serviceid"});
  services.hasMany(service_alarms, { as: "service_alarms", foreignKey: "serviceid"});
  services_links.belongsTo(services, { as: "serviceup", foreignKey: "serviceupid"});
  services.hasMany(services_links, { as: "services_links", foreignKey: "serviceupid"});
  services_links.belongsTo(services, { as: "servicedown", foreignKey: "servicedownid"});
  services.hasMany(services_links, { as: "servicedown_services_links", foreignKey: "servicedownid"});
  services_times.belongsTo(services, { as: "service", foreignKey: "serviceid"});
  services.hasMany(services_times, { as: "services_times", foreignKey: "serviceid"});
  sysmap_shape.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmap_shape, { as: "sysmap_shapes", foreignKey: "sysmapid"});
  sysmap_url.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmap_url, { as: "sysmap_urls", foreignKey: "sysmapid"});
  sysmap_user.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmap_user, { as: "sysmap_users", foreignKey: "sysmapid"});
  sysmap_usrgrp.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmap_usrgrp, { as: "sysmap_usrgrps", foreignKey: "sysmapid"});
  sysmaps_elements.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmaps_elements, { as: "sysmaps_elements", foreignKey: "sysmapid"});
  sysmaps_links.belongsTo(sysmaps, { as: "sysmap", foreignKey: "sysmapid"});
  sysmaps.hasMany(sysmaps_links, { as: "sysmaps_links", foreignKey: "sysmapid"});
  widget_field.belongsTo(sysmaps, { as: "value_sysmap", foreignKey: "value_sysmapid"});
  sysmaps.hasMany(widget_field, { as: "widget_fields", foreignKey: "value_sysmapid"});
  sysmap_element_trigger.belongsTo(sysmaps_elements, { as: "selement", foreignKey: "selementid"});
  sysmaps_elements.hasMany(sysmap_element_trigger, { as: "sysmap_element_triggers", foreignKey: "selementid"});
  sysmap_element_url.belongsTo(sysmaps_elements, { as: "selement", foreignKey: "selementid"});
  sysmaps_elements.hasMany(sysmap_element_url, { as: "sysmap_element_urls", foreignKey: "selementid"});
  sysmaps_element_tag.belongsTo(sysmaps_elements, { as: "selement", foreignKey: "selementid"});
  sysmaps_elements.hasMany(sysmaps_element_tag, { as: "sysmaps_element_tags", foreignKey: "selementid"});
  sysmaps_links.belongsTo(sysmaps_elements, { as: "selementid1_sysmaps_element", foreignKey: "selementid1"});
  sysmaps_elements.hasMany(sysmaps_links, { as: "sysmaps_links", foreignKey: "selementid1"});
  sysmaps_links.belongsTo(sysmaps_elements, { as: "selementid2_sysmaps_element", foreignKey: "selementid2"});
  sysmaps_elements.hasMany(sysmaps_links, { as: "selementid2_sysmaps_links", foreignKey: "selementid2"});
  sysmaps_link_triggers.belongsTo(sysmaps_links, { as: "link", foreignKey: "linkid"});
  sysmaps_links.hasMany(sysmaps_link_triggers, { as: "sysmaps_link_triggers", foreignKey: "linkid"});
  task_acknowledge.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_acknowledge, { as: "task_acknowledge", foreignKey: "taskid"});
  task_check_now.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_check_now, { as: "task_check_now", foreignKey: "taskid"});
  task_close_problem.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_close_problem, { as: "task_close_problem", foreignKey: "taskid"});
  task_data.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_data, { as: "task_datum", foreignKey: "taskid"});
  task_remote_command.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_remote_command, { as: "task_remote_command", foreignKey: "taskid"});
  task_remote_command_result.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_remote_command_result, { as: "task_remote_command_result", foreignKey: "taskid"});
  task_result.belongsTo(task, { as: "task", foreignKey: "taskid"});
  task.hasOne(task_result, { as: "task_result", foreignKey: "taskid"});
  maintenances_windows.belongsTo(timeperiods, { as: "timeperiod", foreignKey: "timeperiodid"});
  timeperiods.hasMany(maintenances_windows, { as: "maintenances_windows", foreignKey: "timeperiodid"});
  functions.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasMany(functions, { as: "functions", foreignKey: "triggerid"});
  services.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasMany(services, { as: "services", foreignKey: "triggerid"});
  sysmap_element_trigger.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasMany(sysmap_element_trigger, { as: "sysmap_element_triggers", foreignKey: "triggerid"});
  sysmaps_link_triggers.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasMany(sysmaps_link_triggers, { as: "sysmaps_link_triggers", foreignKey: "triggerid"});
  trigger_depends.belongsTo(triggers, { as: "triggerid_down_trigger", foreignKey: "triggerid_down"});
  triggers.hasMany(trigger_depends, { as: "trigger_depends", foreignKey: "triggerid_down"});
  trigger_depends.belongsTo(triggers, { as: "triggerid_up_trigger", foreignKey: "triggerid_up"});
  triggers.hasMany(trigger_depends, { as: "triggerid_up_trigger_depends", foreignKey: "triggerid_up"});
  trigger_discovery.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasOne(trigger_discovery, { as: "trigger_discovery", foreignKey: "triggerid"});
  trigger_discovery.belongsTo(triggers, { as: "parent_trigger", foreignKey: "parent_triggerid"});
  triggers.hasMany(trigger_discovery, { as: "parent_trigger_trigger_discoveries", foreignKey: "parent_triggerid"});
  trigger_tag.belongsTo(triggers, { as: "trigger", foreignKey: "triggerid"});
  triggers.hasMany(trigger_tag, { as: "trigger_tags", foreignKey: "triggerid"});
  triggers.belongsTo(triggers, { as: "template", foreignKey: "templateid"});
  triggers.hasMany(triggers, { as: "triggers", foreignKey: "templateid"});
  acknowledges.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(acknowledges, { as: "acknowledges", foreignKey: "userid"});
  alerts.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(alerts, { as: "alerts", foreignKey: "userid"});
  auditlog.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(auditlog, { as: "auditlogs", foreignKey: "userid"});
  dashboard.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(dashboard, { as: "dashboards", foreignKey: "userid"});
  dashboard_user.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(dashboard_user, { as: "dashboard_users", foreignKey: "userid"});
  media.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(media, { as: "media", foreignKey: "userid"});
  opmessage_usr.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(opmessage_usr, { as: "opmessage_usrs", foreignKey: "userid"});
  profiles.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(profiles, { as: "profiles", foreignKey: "userid"});
  report.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(report, { as: "reports", foreignKey: "userid"});
  report_user.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(report_user, { as: "report_users", foreignKey: "userid"});
  report_user.belongsTo(users, { as: "access_user", foreignKey: "access_userid"});
  users.hasMany(report_user, { as: "access_user_report_users", foreignKey: "access_userid"});
  report_usrgrp.belongsTo(users, { as: "access_user", foreignKey: "access_userid"});
  users.hasMany(report_usrgrp, { as: "report_usrgrps", foreignKey: "access_userid"});
  sessions.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(sessions, { as: "sessions", foreignKey: "userid"});
  sysmap_user.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(sysmap_user, { as: "sysmap_users", foreignKey: "userid"});
  sysmaps.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(sysmaps, { as: "sysmaps", foreignKey: "userid"});
  token.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(token, { as: "tokens", foreignKey: "userid"});
  token.belongsTo(users, { as: "creator_user", foreignKey: "creator_userid"});
  users.hasMany(token, { as: "creator_user_tokens", foreignKey: "creator_userid"});
  users_groups.belongsTo(users, { as: "user", foreignKey: "userid"});
  users.hasMany(users_groups, { as: "users_groups", foreignKey: "userid"});
  config.belongsTo(usrgrp, { as: "alert_usrgrp", foreignKey: "alert_usrgrpid"});
  usrgrp.hasMany(config, { as: "configs", foreignKey: "alert_usrgrpid"});
  dashboard_usrgrp.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(dashboard_usrgrp, { as: "dashboard_usrgrps", foreignKey: "usrgrpid"});
  opmessage_grp.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(opmessage_grp, { as: "opmessage_grps", foreignKey: "usrgrpid"});
  report_usrgrp.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(report_usrgrp, { as: "report_usrgrps", foreignKey: "usrgrpid"});
  rights.belongsTo(usrgrp, { as: "group", foreignKey: "groupid"});
  usrgrp.hasMany(rights, { as: "rights", foreignKey: "groupid"});
  scripts.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(scripts, { as: "scripts", foreignKey: "usrgrpid"});
  sysmap_usrgrp.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(sysmap_usrgrp, { as: "sysmap_usrgrps", foreignKey: "usrgrpid"});
  tag_filter.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(tag_filter, { as: "tag_filters", foreignKey: "usrgrpid"});
  users_groups.belongsTo(usrgrp, { as: "usrgrp", foreignKey: "usrgrpid"});
  usrgrp.hasMany(users_groups, { as: "users_groups", foreignKey: "usrgrpid"});
  items.belongsTo(valuemap, { as: "valuemap", foreignKey: "valuemapid"});
  valuemap.hasMany(items, { as: "items", foreignKey: "valuemapid"});
  valuemap_mapping.belongsTo(valuemap, { as: "valuemap", foreignKey: "valuemapid"});
  valuemap.hasMany(valuemap_mapping, { as: "valuemap_mappings", foreignKey: "valuemapid"});
  widget_field.belongsTo(widget, { as: "widget", foreignKey: "widgetid"});
  widget.hasMany(widget_field, { as: "widget_fields", foreignKey: "widgetid"});

  return {
    acknowledges,
    actions,
    alerts,
    auditlog,
    auditlog_details,
    autoreg_host,
    conditions,
    config,
    config_autoreg_tls,
    corr_condition,
    corr_condition_group,
    corr_condition_tag,
    corr_condition_tagpair,
    corr_condition_tagvalue,
    corr_operation,
    correlation,
    dashboard,
    dashboard_page,
    dashboard_user,
    dashboard_usrgrp,
    dbversion,
    dchecks,
    dhosts,
    drules,
    dservices,
    escalations,
    event_recovery,
    event_suppress,
    event_tag,
    events,
    expressions,
    functions,
    globalmacro,
    globalvars,
    graph_discovery,
    graph_theme,
    graphs,
    graphs_items,
    group_discovery,
    group_prototype,
    history,
    history_log,
    history_str,
    history_text,
    history_uint,
    host_discovery,
    host_inventory,
    host_tag,
    hostmacro,
    hosts,
    hosts_groups,
    hosts_templates,
    housekeeper,
    hstgrp,
    httpstep,
    httpstep_field,
    httpstepitem,
    httptest,
    httptest_field,
    httptest_tag,
    httptestitem,
    icon_map,
    icon_mapping,
    ids,
    images,
    interface,
    interface_discovery,
    interface_snmp,
    item_condition,
    item_discovery,
    item_parameter,
    item_preproc,
    item_rtdata,
    item_tag,
    items,
    lld_macro_path,
    lld_override,
    lld_override_condition,
    lld_override_opdiscover,
    lld_override_operation,
    lld_override_ophistory,
    lld_override_opinventory,
    lld_override_opperiod,
    lld_override_opseverity,
    lld_override_opstatus,
    lld_override_optag,
    lld_override_optemplate,
    lld_override_optrends,
    maintenance_tag,
    maintenances,
    maintenances_groups,
    maintenances_hosts,
    maintenances_windows,
    media,
    media_type,
    media_type_message,
    media_type_param,
    module,
    opcommand,
    opcommand_grp,
    opcommand_hst,
    opconditions,
    operations,
    opgroup,
    opinventory,
    opmessage,
    opmessage_grp,
    opmessage_usr,
    optemplate,
    problem,
    problem_tag,
    profiles,
    proxy_autoreg_host,
    proxy_dhistory,
    proxy_history,
    regexps,
    report,
    report_param,
    report_user,
    report_usrgrp,
    rights,
    role,
    role_rule,
    script_param,
    scripts,
    service_alarms,
    services,
    services_links,
    services_times,
    sessions,
    sysmap_element_trigger,
    sysmap_element_url,
    sysmap_shape,
    sysmap_url,
    sysmap_user,
    sysmap_usrgrp,
    sysmaps,
    sysmaps_element_tag,
    sysmaps_elements,
    sysmaps_link_triggers,
    sysmaps_links,
    tag_filter,
    task,
    task_acknowledge,
    task_check_now,
    task_close_problem,
    task_data,
    task_remote_command,
    task_remote_command_result,
    task_result,
    timeperiods,
    token,
    trends,
    trends_uint,
    trends_uint-test,
    trigger_depends,
    trigger_discovery,
    trigger_queue,
    trigger_tag,
    triggers,
    users,
    users_groups,
    usrgrp,
    valuemap,
    valuemap_mapping,
    widget,
    widget_field,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
