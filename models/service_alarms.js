const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('service_alarms', {
    servicealarmid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    serviceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'services',
        key: 'serviceid'
      }
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'service_alarms',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "servicealarmid" },
        ]
      },
      {
        name: "service_alarms_1",
        using: "BTREE",
        fields: [
          { name: "serviceid" },
          { name: "clock" },
        ]
      },
      {
        name: "service_alarms_2",
        using: "BTREE",
        fields: [
          { name: "clock" },
        ]
      },
    ]
  });
};
