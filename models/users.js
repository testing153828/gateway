const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: "",
      unique: "users_1"
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: ""
    },
    surname: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: ""
    },
    passwd: {
      type: DataTypes.STRING(60),
      allowNull: false,
      defaultValue: ""
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    autologin: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    autologout: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "15m"
    },
    lang: {
      type: DataTypes.STRING(7),
      allowNull: false,
      defaultValue: "default"
    },
    refresh: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "30s"
    },
    theme: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: "default"
    },
    attempt_failed: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    attempt_ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    attempt_clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    rows_per_page: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 50
    },
    timezone: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "default"
    },
    roleid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'role',
        key: 'roleid'
      }
    }
  }, {
    sequelize,
    tableName: 'users',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "users_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "username" },
        ]
      },
      {
        name: "c_users_1",
        using: "BTREE",
        fields: [
          { name: "roleid" },
        ]
      },
    ]
  });
};
