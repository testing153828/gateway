const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('host_inventory', {
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    inventory_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    type_full: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    alias: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    os: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    os_full: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    os_short: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    serialno_a: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    serialno_b: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    tag: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    asset_tag: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    macaddress_a: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    macaddress_b: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    hardware: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    hardware_full: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    software: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    software_full: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    software_app_a: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    software_app_b: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    software_app_c: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    software_app_d: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    software_app_e: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    contact: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    location: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    location_lat: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ""
    },
    location_lon: {
      type: DataTypes.STRING(16),
      allowNull: false,
      defaultValue: ""
    },
    notes: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    chassis: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    model: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    hw_arch: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    },
    vendor: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    contract_number: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    installer_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    deployment_status: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    url_a: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    url_b: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    url_c: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    host_networks: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    host_netmask: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    host_router: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    oob_ip: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    oob_netmask: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    oob_router: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    date_hw_purchase: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    date_hw_install: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    date_hw_expiry: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    date_hw_decomm: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    site_address_a: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    site_address_b: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    site_address_c: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    site_city: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    site_state: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    site_country: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    site_zip: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    site_rack: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    site_notes: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    poc_1_name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_email: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_phone_a: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_phone_b: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_cell: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_screen: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_1_notes: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    poc_2_name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_email: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_phone_a: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_phone_b: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_cell: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_screen: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    poc_2_notes: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'host_inventory',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "hostid" },
        ]
      },
    ]
  });
};
