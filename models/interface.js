const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('interface', {
    interfaceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    main: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    useip: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    ip: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "127.0.0.1"
    },
    dns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    port: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "10050"
    },
    available: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    error: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    errors_from: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    disable_until: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'interface',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "interfaceid" },
        ]
      },
      {
        name: "interface_1",
        using: "BTREE",
        fields: [
          { name: "hostid" },
          { name: "type" },
        ]
      },
      {
        name: "interface_2",
        using: "BTREE",
        fields: [
          { name: "ip" },
          { name: "dns" },
        ]
      },
      {
        name: "interface_3",
        using: "BTREE",
        fields: [
          { name: "available" },
        ]
      },
    ]
  });
};
