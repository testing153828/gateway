const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('history_log', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    timestamp: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    source: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    severity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    logeventid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'history_log',
    timestamps: false,
    indexes: [
      {
        name: "history_log_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "clock" },
        ]
      },
    ]
  });
};
