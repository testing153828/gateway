const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('items', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    snmp_oid: {
      type: DataTypes.STRING(512),
      allowNull: false,
      defaultValue: ""
    },
    hostid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hosts',
        key: 'hostid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    key_: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    delay: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: "0"
    },
    history: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "90d"
    },
    trends: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "365d"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    trapper_hosts: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    units: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    formula: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    logtimefmt: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    templateid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    valuemapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'valuemap',
        key: 'valuemapid'
      }
    },
    params: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    ipmi_sensor: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    authtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    username: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    publickey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    privatekey: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    flags: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    interfaceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'interface',
        key: 'interfaceid'
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    inventory_link: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lifetime: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "30d"
    },
    evaltype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    jmx_endpoint: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    master_itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'items',
        key: 'itemid'
      }
    },
    timeout: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "3s"
    },
    url: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    query_fields: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    posts: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status_codes: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "200"
    },
    follow_redirects: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    post_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    http_proxy: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    headers: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    retrieve_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    request_method: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    output_format: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    ssl_cert_file: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ssl_key_file: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    ssl_key_password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ""
    },
    verify_peer: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    verify_host: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    allow_traps: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    discover: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    uuid: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'items',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "itemid" },
        ]
      },
      {
        name: "items_1",
        using: "BTREE",
        fields: [
          { name: "hostid" },
          { name: "key_", length: 1021 },
        ]
      },
      {
        name: "items_3",
        using: "BTREE",
        fields: [
          { name: "status" },
        ]
      },
      {
        name: "items_4",
        using: "BTREE",
        fields: [
          { name: "templateid" },
        ]
      },
      {
        name: "items_5",
        using: "BTREE",
        fields: [
          { name: "valuemapid" },
        ]
      },
      {
        name: "items_6",
        using: "BTREE",
        fields: [
          { name: "interfaceid" },
        ]
      },
      {
        name: "items_7",
        using: "BTREE",
        fields: [
          { name: "master_itemid" },
        ]
      },
      {
        name: "items_8",
        using: "BTREE",
        fields: [
          { name: "key_", length: 1024 },
        ]
      },
    ]
  });
};
