const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('history', {
    itemid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false
    },
    clock: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    ns: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'history',
    timestamps: false,
    indexes: [
      {
        name: "history_1",
        using: "BTREE",
        fields: [
          { name: "itemid" },
          { name: "clock" },
        ]
      },
    ]
  });
};
