const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('corr_condition_group', {
    corr_conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'corr_condition',
        key: 'corr_conditionid'
      }
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    groupid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'hstgrp',
        key: 'groupid'
      }
    }
  }, {
    sequelize,
    tableName: 'corr_condition_group',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "corr_conditionid" },
        ]
      },
      {
        name: "corr_condition_group_1",
        using: "BTREE",
        fields: [
          { name: "groupid" },
        ]
      },
    ]
  });
};
