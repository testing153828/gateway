const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sysmaps_links', {
    linkid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    sysmapid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps',
        key: 'sysmapid'
      }
    },
    selementid1: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps_elements',
        key: 'selementid'
      }
    },
    selementid2: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'sysmaps_elements',
        key: 'selementid'
      }
    },
    drawtype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    color: {
      type: DataTypes.STRING(6),
      allowNull: false,
      defaultValue: "000000"
    },
    label: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'sysmaps_links',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "linkid" },
        ]
      },
      {
        name: "sysmaps_links_1",
        using: "BTREE",
        fields: [
          { name: "sysmapid" },
        ]
      },
      {
        name: "sysmaps_links_2",
        using: "BTREE",
        fields: [
          { name: "selementid1" },
        ]
      },
      {
        name: "sysmaps_links_3",
        using: "BTREE",
        fields: [
          { name: "selementid2" },
        ]
      },
    ]
  });
};
