const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('conditions', {
    conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    actionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'actions',
        key: 'actionid'
      }
    },
    conditiontype: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value2: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'conditions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "conditionid" },
        ]
      },
      {
        name: "conditions_1",
        using: "BTREE",
        fields: [
          { name: "actionid" },
        ]
      },
    ]
  });
};
