const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_opperiod', {
    lld_override_operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'lld_override_operation',
        key: 'lld_override_operationid'
      }
    },
    delay: {
      type: DataTypes.STRING(1024),
      allowNull: false,
      defaultValue: "0"
    }
  }, {
    sequelize,
    tableName: 'lld_override_opperiod',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_operationid" },
        ]
      },
    ]
  });
};
