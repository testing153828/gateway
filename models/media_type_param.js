const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('media_type_param', {
    mediatype_paramid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    mediatypeid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'media_type',
        key: 'mediatypeid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'media_type_param',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "mediatype_paramid" },
        ]
      },
      {
        name: "media_type_param_1",
        using: "BTREE",
        fields: [
          { name: "mediatypeid" },
        ]
      },
    ]
  });
};
