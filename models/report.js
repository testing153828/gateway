const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report', {
    reportid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      unique: "report_1"
    },
    description: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dashboardid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'dashboard',
        key: 'dashboardid'
      }
    },
    period: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    cycle: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    weekdays: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    start_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    active_since: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    active_till: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    lastsent: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    info: {
      type: DataTypes.STRING(2048),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'report',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "reportid" },
        ]
      },
      {
        name: "report_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "c_report_1",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
      {
        name: "c_report_2",
        using: "BTREE",
        fields: [
          { name: "dashboardid" },
        ]
      },
    ]
  });
};
