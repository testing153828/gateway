const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_optag', {
    lld_override_optagid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    lld_override_operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'lld_override_operation',
        key: 'lld_override_operationid'
      }
    },
    tag: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'lld_override_optag',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_optagid" },
        ]
      },
      {
        name: "lld_override_optag_1",
        using: "BTREE",
        fields: [
          { name: "lld_override_operationid" },
        ]
      },
    ]
  });
};
