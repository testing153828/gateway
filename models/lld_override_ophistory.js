const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lld_override_ophistory', {
    lld_override_operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'lld_override_operation',
        key: 'lld_override_operationid'
      }
    },
    history: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "90d"
    }
  }, {
    sequelize,
    tableName: 'lld_override_ophistory',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lld_override_operationid" },
        ]
      },
    ]
  });
};
