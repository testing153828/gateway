const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('usrgrp', {
    usrgrpid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: "",
      unique: "usrgrp_1"
    },
    gui_access: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    users_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    debug_mode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'usrgrp',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "usrgrpid" },
        ]
      },
      {
        name: "usrgrp_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
};
