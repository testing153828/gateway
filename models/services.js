const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('services', {
    serviceid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(128),
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    algorithm: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    triggerid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      references: {
        model: 'triggers',
        key: 'triggerid'
      }
    },
    showsla: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    goodsla: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 99.9
    },
    sortorder: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'services',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "serviceid" },
        ]
      },
      {
        name: "services_1",
        using: "BTREE",
        fields: [
          { name: "triggerid" },
        ]
      },
    ]
  });
};
