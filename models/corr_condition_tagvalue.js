const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('corr_condition_tagvalue', {
    corr_conditionid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'corr_condition',
        key: 'corr_conditionid'
      }
    },
    tag: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    operator: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'corr_condition_tagvalue',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "corr_conditionid" },
        ]
      },
    ]
  });
};
