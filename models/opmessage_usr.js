const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('opmessage_usr', {
    opmessage_usrid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    operationid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'operationid'
      }
    },
    userid: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      references: {
        model: 'users',
        key: 'userid'
      }
    }
  }, {
    sequelize,
    tableName: 'opmessage_usr',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "opmessage_usrid" },
        ]
      },
      {
        name: "opmessage_usr_1",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "operationid" },
          { name: "userid" },
        ]
      },
      {
        name: "opmessage_usr_2",
        using: "BTREE",
        fields: [
          { name: "userid" },
        ]
      },
    ]
  });
};
