const axios = require('axios');


let cust_info = {
    BaerFurniture2 : {
        customerid: '8cbbekaeyg9u',
        clientID: '740e794b-da91-4136-9e22-c6d56758cedc',
        clientSecret: 'grWzc35Rc9AfIorhxJObRg=='
    },
    AJManufacturing : {
        customerid: 'ChoiceSoluti',
        clientID: process.env.CTX_Cloud_clientId,
        clientSecret: process.env.CTX_Cloud_clientSecret
    },
    ChoiceCloud: {
        customerid: 'ChoiceSoluti',
        clientID: process.env.CTX_Cloud_clientId,
        clientSecret: process.env.CTX_Cloud_clientSecret
    },
    TicketsforLess : {
        customerid: 'aug5czwzvwfr',
        clientID: '970b63c9-1b28-4305-8e21-8527f4f92b2f',
        clientSecret: 'xoXQbLTBfvQ3NBKnBeNwLg=='       
    },
    ACEAuditingComplianceEduc : {
        customerid: 'tmlj9ctbyr03',
        clientID: '54128090-9fd4-4b43-acc0-53eef3088f9f',
        clientSecret: 'uPD6oTuePO9mLQbtO7itiQ=='
    },
    ChoiceDev : {
        customerid: 'dimuyzcbwjof ',
        clientID: 'ecd0dc2a-2df3-4be3-b93d-58cae63491c5',
        clientSecret: '3l0YBQIoIv73wN1zICQNKQ=='
    },
    ChoiceSolutions : {
        customerid: 'be4258lg1b38',
        clientID: '3054eb70-f338-4fad-bf77-0a6805d627fc',
        clientSecret: 'UyQzTGILziXT_Bh8WUoi8A=='
    },
    PediatricDermatologyofKan : {
        customerid: 'uwyq370i8rx8',
        clientID: '9c23b7c7-6554-4b5d-b515-625bba858777',
        clientSecret: 'QNxX-M3JZHakjbfOKL5IOQ=='
    },
    SageDental: {
        customerid: 'i8w7gx25kxnj',
        clientID: '22079d88-cbe5-474f-a2eb-6c9fbb5a0119',
        clientSecret: 'FfOGvwqRUI_ljEzP0-_ikg=='
    },
    SoleoHealth: {
        customerid: 's5t9y4gnpziv',
        clientID: 'db71f0aa-71e3-455d-935a-1dda543c7fbf',
        clientSecret: 'wxGZMyyvXDIF3Z-Pz5moeQ=='
    }
}

let customerid = "ChoiceSoluti";
const {
    globalGatewayCache
} = require("./cache.js");
const lifeCicle = {
    0: 'Active',
    1: 'Deleted',
    2: 'RequiresResolution',
    3: 'Stub',
}
const sessionSupportCode = {
    0: 'Unknown',
    1: 'SingleSession',
    2: 'MultiSession'
}
const citrixMonitorModelEnum = {
 
    'Catalogs': {
        'AllocationType': {
            0: 'Unknown',
            1: 'Static',
            2: 'Random',
            3: 'Permanent'
        },
        'ProvisioningType': {
            0: 'Unknown',
            1: 'MVS',
            2: 'PVS',
            3: 'Manual'
        },
        'MachineRole': {
            0: 'Vda',
            1: 'Ddc',
            2: 'Both'
        },
        'SessionSupportCode': {...sessionSupportCode},
        'LifecycleState': {...lifeCicle}
        
    },
    'DesktopGroups' : {
        'DesktopKind': {
            0: 'Private',
            1: 'Shared'
        },
        'LifecycleState': {...lifeCicle},
        'SessionSupportCode': {...sessionSupportCode},
        'DeliveryType' : {
            0: 'DesktopOnly',
            1: 'AppsOnly',
            2: 'DesktopAndApps'
        }
    },
    'Machines' : {
        'MachineFaultStateCode': {
            0: 'Unknown',
            1: 'None',
            2: 'FailedToStart',
            3: 'StuckOnBoot',
            4: 'Unregistered',
            5: 'MaxCapacity',
            6: 'VirtualMachineNotFound'
        },
        'LifecycleState': {...lifeCicle}
    },
    'TaskLogs': {
        'TaskCategory': {
            0: 'Unknown',
            1: 'Consolidation',
            2: 'Reaping',
            3: 'Grooming',
            4: 'Cas'
        },
        'TaskResults': {
            0: 'Unknown',
            1: 'Success',
            2: 'Failure',
            3: 'Skipped'
        }
    }

}

async function apiCustBaerer(cwCustId) {
    let sites = []; // Initialize sites to an empty array
    let token = '';
    

    if(globalGatewayCache.has(`citrix_Baerer_${cwCustId}`)){
        console.log(`returning ${cwCustId} citrix Baerer from Cache`);
        token = globalGatewayCache.get(`citrix_Baerer_${cwCustId}`)            
    }
    else{
        try {
            const url = `https://api-us.cloud.com/cctrustoauth2/${cust_info[cwCustId]['customerid']}/tokens/clients`;
            
            // Request token
            const tokenHeaders = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Accept-Encoding': 'gzip, deflate, br'
            };

            const tokenData = new URLSearchParams();
            tokenData.append('grant_type', 'client_credentials');
            tokenData.append('client_id', cust_info[cwCustId]['clientID']);
            tokenData.append('client_secret', cust_info[cwCustId]['clientSecret']);

            const tokenResponse = await axios.post(url, tokenData, { headers: tokenHeaders });
            token = tokenResponse.data.access_token;
            globalGatewayCache.set(`citrix_Baerer_${cwCustId}`, token);
        }catch (error) {
            handleAxiosError(error);
        }
    }

    if(globalGatewayCache.has(`citrix_SiteID_${cwCustId}`)){
        console.log(`returning ${cwCustId} citrix SiteId from Cache`);
        sites = globalGatewayCache.get(`citrix_SiteID_${cwCustId}`)            
    }
    else{
        // Request customer site information
        const siteidURL = 'https://api.cloud.com/cvad/manage/me';
        const siteidHeaders = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            'Citrix-CustomerId': cust_info[cwCustId]['customerid'],
            'Authorization': `CWSAuth bearer=${token}`
        };

        try {
            const siteIdResponse = await axios.get(siteidURL, { headers: siteidHeaders });
            sites = siteIdResponse.data.Customers[0].Sites;
            globalGatewayCache.set(`citrix_SiteID_${cwCustId}`, sites) 
            // Output the Sites IDs to the console
            sites.forEach(site => {
                console.log('Site ID:', site.Id);
            });

        } catch (error) {
            handleAxiosError(error);
        }
    }

        // Return token and site information
    const Cust_info = {
        'token': token,
        'Customer-Site': sites.length > 0 ? sites[0].Id : null // Check if sites is empty
    };

    return Cust_info;
}

function handleAxiosError(error) {
    if (error.response) {
        console.error('Server responded with status', error.response.status);
        console.error('Response data:', error.response.data);
    } else if (error.request) {
        console.error('No response received from the server');
    } else {
        console.error('Error setting up the request:', error.message);
    }
}

async function citrixCloudLic(product) {
    const Token = await apiCustBaerer('ChoiceCloud');
    const citrixToken = Token['token'];
    var dateObj = new Date();
    var month = ("0" + (dateObj.getMonth())).slice(-2); 
    var year = dateObj.getUTCFullYear();
    if (month === "00"){
        month = "12";
        year = (parseInt(year, 10)-1).toString();
    } else{
        month = ("0" + (dateObj.getMonth())).slice(-2);
    }
    var cvadDate = year + '-' + month;
    switch (product){
        case 'cvad':
            url = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/licenseusages/deployments/cloud/memberships/csp/products/cvad?date='+ cvadDate;
            break;
        case 'daas':
            url = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/licenseusages/deployments/cloud/memberships/csp/products/daas?date='+ cvadDate;
            break;
        case 'old':
            url = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/license/csp/onpremise/cvad/usage/?date='+ cvadDate + '&displayName=Avyve'
            break;
        case 'adc':
            url = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/netscalercallinghomedata'
           break;
        case 'lic':
            url = 'https://licensing-eastus-release-b.citrixworkspacesapi.net/ChoiceSoluti/customerusermap?date='+ cvadDate;
            break;
    }


try{
    let conf = {
        method: 'get',
        url: url,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'CwsAuth Bearer=' + citrixToken,
            'Accept-Encoding': 'gzip, deflate, br'
        }
    };
    let response1 = await axios(conf);
    let data = response1.data;
    globalGatewayCache.set("citrix_CloudLic", data);
    return(data);

} catch(error) {
    /**************************************************************************
    TODO: If error is "Invalid bearer token" then expire token cache and re-try
    {
        "Error": {
            "Type": "CWSAuthException",
            "Message": "Invalid bearer token."
        } 
    } 
    **************************************************************************/
    return(error);
}
   // }
}

function transformToZabbixFormat(data) {
    const transformedDataArray = [];
    tdata = data.Items ? data.Items : data;
    const dataArray = Array.isArray(tdata) ? tdata : [tdata]; // Ensure dataArray is an array

    function escapeJsonValue(value) {
        if (typeof value === 'string') {
            return JSON.stringify(value);
        }
        return value;
    }

    function flattenObject(obj, prefix = '') {
        const flattenedObj = {};
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const newKey = prefix ? `${prefix}.${key.toUpperCase()}` : key.toUpperCase();
                if (typeof obj[key] === 'object' && !Array.isArray(obj[key]) && obj[key] !== null) {
                    Object.assign(flattenedObj, flattenObject(obj[key], newKey));
                } else {
                    flattenedObj[`{#${newKey}}`] = obj[key];
                }
            }
        }
        return flattenedObj;
    }


    dataArray.forEach((item, index) => {
        const flattenedItem = flattenObject(item);
        transformedDataArray.push(flattenedItem);
    });

    return transformedDataArray;
}

function transform ( transformed_data, json, type) {
    let transformed =  {}
    if (type == 'multiTenantsUsage'){
        transformed = {
        "type" : 'multiTenantsUsage',
        "customerId" : "6y19m9bf7nf9",
        "orgId" : 51612561,
        "displayName" : "A.J. Manufacturing Company, Inc.",
        "editionName": json.editionName || "N/A",
        "totalCommitCount": "N/A",
        "totalUsageCount": json.totalAssignedMultiUsers,
        "totalOverageCount": "N/A",
        "totalUsagePercent": "N/A"
        }
        transformed_data["tenant"].push(transformed);
    }
    else{
        json[type].forEach((tenant) => {
            transformed = {
                "type" : type,
                "customerId": tenant["customerId"],
                "orgId": tenant["orgId"],
                "displayName": tenant["displayName"],
                "editionName": tenant["editionName"] || "N/A",
                "totalCommitCount":tenant["totalCommitCount"] || "N/A",
                "totalUsageCount": tenant["totalUsageCount"],
                "totalOverageCount": tenant["totalOverageCount"] || "N/A",
                "totalUsagePercent": tenant["totalUsagePercent"] || "N/A",
            }
        
            transformed_data["tenant"].push(transformed);
        })
    }
    return transformed_data
}

module.exports = {
    transform,transformToZabbixFormat,citrixCloudLic,apiCustBaerer,cust_info
};



