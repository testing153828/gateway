const axios = require('axios');
const NodeCache = require( "node-cache" );
const perchCache = new NodeCache( { stdTTL: 100, checkperiod: 120 } );


const perchVars = {
    'perchClient_id': process.env.perch_Client_id,
    'perchSecret': process.env.perch_Secret,
    'perchAudience': process.env.perch_Audience,
    'x-api-key': process.env.perch_xApiKey,
    'companiesURL': process.env.perch_companiesURL
}

async function getBaerer() {
    if(perchCache.has("baerer")){
        console.log("returning from Cache");
        console.log(perchCache.get("baerer"));
        console.log(perchCache.getTtl("baerer"));
        return(perchCache.get("baerer"));
    }
    else{
        console.log("getting a fresh token");
        try {
            const config = {
                method: 'post',
                url: 'https://api.perchsecurity.com/auth/access_token',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'x-api-key': perchVars['x-api-key'] 
                },
                data: {
                    client_id: perchVars.perchClient_id, 
                    client_secret: perchVars.perchSecret,
                    audience: perchVars.perchAudience 
                }
            };
            let res = await axios(config);


           let access = res.data.token_type + ' ' +res.data.access_token
            perchCache.set("baerer", access);
            perchCache.ttl("baerer", res.data.expires_in);
            console.log(perchCache.getTtl("baerer"));
            
            return (access);
        } catch (error) {
            console.error(error);
        };
    }  
};

async function getCompanies() {
    const token = await getBaerer()
    if(perchCache.has("companies")){
        console.log("returning Companies from Cache");
        console.log(perchCache.getTtl("companies"));
        return(perchCache.get("companies"));
    }
    else{
        console.log("getting a fresh Company List");
        try{
            var conf = {
                method: 'get',
                url: perchVars.companiesURL,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'x-api-key': perchVars['x-api-key'],
                    'Authorization': token
                }
            }
            let resp = await axios(conf);
            perchCache.set("companies", resp.data);
            perchCache.ttl("companies", 3600);
            return(resp.data);
        } catch (error) {
            console.error(error);
            return(error);
        }
    }
}

module.exports = {
    getCompanies
};



