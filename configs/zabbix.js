const axios = require('axios');

async function zabbixLogin() {
    try {
        const config = {
            method: 'post',
            url: 'https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php',

            header: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Host': 'zmonitoring.choicecloud.com',
                'Accept-Encoding': 'gzip, deflate, br',
                'Connection': 'keep-alive',
            },
            data: {

                "jsonrpc": "2.0",
                "method": "user.login",
                "id": 1,
                "params": { "user": process.env.ZBX_apiUser, "password": process.env.ZBX_apiPass }
            }
        };

        var res = axios(config);
        return (res);

    } catch (error) {
        console.log('zabbixlogin fruntion error ' + error);
    }

}

//connection
const zmConfig = {
    "user": process.env.ZBX_dbUser,
    "host": process.env.ZBX_dbHost,
    "port": parseInt(process.env.ZBX_dbPort, 10),
   // "port": process.env.ZBX_dbPort,
    "password": process.env.ZBX_dbPass,
    "database": process.env.ZBX_db,
    "connectionslimit": 10
};

const billHistory = {
    "user": process.env.ZBX_dbUser,
    "host": process.env.ZBX_dbHost,
    "port": parseInt(process.env.ZBX_dbPort, 10),
   // "port": process.env.ZBX_dbPort,
    "password": process.env.ZBX_dbPass,
    "database": 'billing_history',
    "connectionslimit": 10
}




//SQL Statements

const zqueries = {
    billLastMonth : "SELECT Monitor_Company_ID,Company_Name, Company_ID, Company_RecID, Sku, ItemName, Monitor, sum(Qty) FROM  billing_history.monitoring_history WHERE `Year` = YEAR(CURRENT_DATE() - INTERVAL 1 Month) and `Month` = MONTH(CURRENT_DATE() - INTERVAL 1 MONTH) GROUP BY  `Company_ID`, `Company_RecID`, `Company_Name`, `Sku`, `Monitor_Company_ID`, `ItemName`, `Monitor`",
    billConsolidatedLastMonth : "SELECT  Company_Name, Company_ID, Company_RecID, Sku, sum(Qty) FROM  billing_history.monitoring_history WHERE `Year` = YEAR(CURRENT_DATE() - INTERVAL 1 Month) and `Month` = MONTH(CURRENT_DATE() - INTERVAL 1 MONTH) GROUP BY `Company_ID`, `Company_RecID`, `Company_Name`, `Sku` ORDER BY Company_RecID",
    sku_list: "Select * from billing_history.SKU",
    billtableQC: `UPDATE billing_history.monitoring_history
                    JOIN billing_history.Company ON monitoring_history.Company_ID = Company.Company_ID
                                    OR monitoring_history.Company_Name = Company.Company_Name
                                    OR monitoring_history.Company_RecID = Company.Company_RecID
                    SET monitoring_history.Company_ID = Company.Company_ID,
                        monitoring_history.Company_Name = Company.Company_Name,
                        monitoring_history.Company_RecID = Company.Company_RecID
                    WHERE monitoring_history.Company_ID IS NULL
                    OR monitoring_history.Company_Name IS NULL
                    OR monitoring_history.Company_RecID IS NULL`,
    full: `
    Select Company_ID,qty,ServerName,clock,ItemName,sku,key_ from (
            
        with  
        itmeselect as (
                        Select * from items wi where 
                        wi.name = '/_Stateless: Used space'
                                        or wi.key_  REGEXP  '^vfs.fs.size|NumberOfLogicalProcessors' 
                                    or
                                        wi.key_ = 'vm.memory.size[total]' or 
                                        wi.name = '/_Stateless: Total space' 
        ),
        uint as (
                SELECT * FROM zabbix.trends_uint u 
                where 
                u.clock BETWEEN unix_timestamp(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00')) 
                AND unix_timestamp(DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59'))
        ),
        proxyID as (
            select hosts.hostid from zabbix.hosts where hosts.host like 'DFW%' or hosts.host like 'dfw%' or hosts.host like 'ATL%ZABBIX01' or hosts.host like 'atl%zabbix01'
        )
        
        SELECT  
            ht.value as 'Company_ID'
            ,Case
                when it.key_ like 'system.cpu.num[snmp]' or it.key_ like '%LogicalProc%' then  max(u.value_max)
            else ROUND( (max(u.value_max) /1073741824), 2 )
            END as 'qty'
            ,hst.name as 'ServerName'
            ,from_unixtime(max(u.clock)) as 'clock'
            ,it.name as 'ItemName'
            ,CASE
            when it.key_ like 'vfs.fs.size%' AND it.name like '%Used space' THEN 'CC - Tier 1 Storage'
            when it.key_ like 'vfs.fs%' AND it.name like '%Total space' THEN 'CC - Tier 1 Total Storage'
            when it.key_ like '%hrStorageUse%' then 'CC - Tier 2 Storage'
            when it.key_ like 'vm.memory.size[total%'  THEN 'CC - vMemory'
            else 'CC - vCPU'
            END as sku
            ,it.key_
            
            FROM proxyID p  
            join zabbix.hosts hst on p.hostid = hst.proxy_hostid 
            join itmeselect it  on  it.hostid = hst.hostid
            right join uint u on u.itemid = it.itemid
            join host_tag ht on hst.hostid = ht.hostid
            
            where 
                hst.status=0
                and ht.tag = 'Company'
                and (ht.value != 'AJManufacturing' or hst.host = 'DFWAJMUEB01' or hst.host = 'ATLAJMFUEB02')
            group by u.itemid,ht.value,it.key_,hst.hostid,hst.name,it.name
            order by ht.value,hst.hostid,sku) as t

    UNION

    Select 
        ht.value as Company_ID
        ,1 as qty
        ,ho.name as ServerName
        ,now() as clock
        ,hsg.name as ItemName
        ,group_concat(distinct(hsg.name) separator ';') as sku
        ,'N/A' as key_ 
        
        from zabbix.hosts ho
            right join zabbix.host_tag ht on ho.hostid = ht.hostid
            right join zabbix.hosts_groups hg on hg.hostid = ho.hostid
            right join zabbix.hstgrp hsg on hsg.groupid = hg.groupid
        where 
            ho.status = 0 
            and ho.proxy_hostid in (SELECT hostid FROM zabbix.hosts where status=5)
            and ht.tag = 'Company'
            and hsg.name in (SELECT name FROM zabbix.hstgrp where name like 'MS -%' or name like 'SECaas%') 
        group by ho.hostid,ht.value,ho.name,hsg.name
        order by Company_ID;
`,
        // Select Company_ID,qty,ServerName,clock,ItemName,sku,key_ from (
            
        //     with  
        //     itmeselect as (
        //                     Select * from items wi where 
        //                     wi.name = '/_Stateless: Used space'
        //                                     or wi.key_  REGEXP  '^vfs.fs.size|NumberOfLogicalProcessors' 
        //                                 or
        //                                     wi.key_ = 'vm.memory.size[total]' or 
        //                                     wi.name = '/_Stateless: Total space' 
        //     ),
        //     uint as (
        //             SELECT * FROM zabbix.trends_uint u 
        //             where 
        //             u.clock BETWEEN unix_timestamp(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00')) 
        //             AND unix_timestamp(DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59'))
        //     )
            
        //     SELECT  
        //         ht.value as 'Company_ID'
        //         ,Case
        //             when it.key_ like 'system.cpu.num[snmp]' or it.key_ like '%LogicalProc%' then  max(u.value_max)
        //         else ROUND( (max(u.value_max) /1073741824), 2 )
        //         END as 'qty'
        //         ,hst.name as 'ServerName'
        //         ,from_unixtime(max(u.clock)) as 'clock'
        //         ,it.name as 'ItemName'
        //         ,CASE
        //         when it.key_ like 'vfs.fs.size%' AND it.name like '%Used space' THEN 'CC - Tier 1 Storage'
        //         when it.key_ like 'vfs.fs%' AND it.name like '%Total space' THEN 'CC - Tier 1 Total Storage'
        //         when it.key_ like '%hrStorageUse%' then 'CC - Tier 2 Storage'
        //         when it.key_ like 'vm.memory.size[total%'  THEN 'CC - vMemory'
        //         else 'CC - vCPU'
        //         END as sku
        //         ,it.key_
                
        //         FROM uint u 
        //         right join itmeselect it on u.itemid = it.itemid
        //         join zabbix.hosts hst on  it.hostid = hst.hostid
        //         join host_tag ht on hst.hostid = ht.hostid
        //         where 
        //             hst.proxy_hostid in (select hosts.hostid from zabbix.hosts where hosts.host like 'DFW%' or hosts.host like 'dfw%' or hosts.host like 'ATL%ZABBIX01' or hosts.host like 'atl%zabbix01') 
        //             and hst.status=0
        //             and ht.tag = 'Company'
        //             and (ht.value != 'AJManufacturing' or hst.host = 'DFWAJMUEB01' or hst.host = 'ATLAJMFUEB02')
        //         group by u.itemid,ht.value,it.key_,hst.hostid,hst.name,it.name
        //         order by ht.value,hst.hostid,sku) as t

        // UNION

        // Select 
        //     ht.value as Company_ID
        //     ,1 as qty
        //     ,ho.name as ServerName
        //     ,now() as clock
        //     ,hsg.name as ItemName
        //     ,group_concat(distinct(hsg.name) separator ';') as sku
        //     ,'N/A' as key_ 
            
        //     from zabbix.hosts ho
        //         right join zabbix.host_tag ht on ho.hostid = ht.hostid
        //         right join zabbix.hosts_groups hg on hg.hostid = ho.hostid
        //         right join zabbix.hstgrp hsg on hsg.groupid = hg.groupid
        //     where 
        //         ho.status = 0 
        //         and ho.proxy_hostid in (SELECT hostid FROM zabbix.hosts where status=5)
        //         and ht.tag = 'Company'
        //         and hsg.name in (SELECT name FROM zabbix.hstgrp where name like 'MS -%' or name like 'SECaas%') 
        //     group by ho.hostid,ht.value,ho.name,hsg.name
        //     order by Company_ID;`,

    unitrends: `        Select Company_ID,qty,ServerName,clock,ItemName,sku,key_ from (
            
        with  
        itmeselect as (
                        Select * from items wi where 
                        wi.name = '/_Stateless: Used space'
                                        or wi.key_  REGEXP  '^vfs.fs.size|NumberOfLogicalProcessors' 
                                    or
                                        wi.key_ = 'vm.memory.size[total]' or 
                                        wi.key_ = 'system.cpu.num[snmp]' or
                                        wi.key_ = 'vm.memory.total[memTotalReal.0]'
        ),
        uint as (
                SELECT * FROM zabbix.trends_uint u 
                where 
                u.clock BETWEEN unix_timestamp(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00')) 
                AND unix_timestamp(DATE_FORMAT(LAST_DAY(NOW()), '%Y-%m-%d 23:59:59'))
        )
        
        SELECT  
            ht.value as 'Company_ID'
            ,Case
                when it.key_ like 'system.cpu.num[snmp]' or it.key_ like '%LogicalProc%' then  max(u.value_max)
            else ROUND( (max(u.value_max) /1073741824), 2 )
            END as 'qty'
            ,hst.name as 'ServerName'
            ,from_unixtime(max(u.clock)) as 'clock'
            ,it.name as 'ItemName'
            ,CASE
            when it.key_ like 'vfs.fs.size%' AND it.name like '%Used space' THEN 'CC - Tier 1 Storage'
            when it.key_ like 'vfs.fs%' AND it.name like '%Total space' THEN 'CC - Tier 1  Total Storage'
            when it.key_ like '%hrStorageUse%' then 'CC - Tier 2 Storage'
            when it.key_ like 'vm.memory.size[total%'  THEN 'CC - vMemory'
            else 'CC - vCPU'
            END as sku
            ,it.key_
            
            FROM uint u 
            right join itmeselect it on u.itemid = it.itemid
            join zabbix.hosts hst on  it.hostid = hst.hostid
            join host_tag ht on hst.hostid = ht.hostid
            where 
                hst.proxy_hostid in (select hosts.hostid from zabbix.hosts where hosts.host like 'DFW%ZABBIX01' or hosts.host like 'ATL%ZABBIX01') 
                and hst.status=0
                and ht.tag = 'Company'
                and hst.name like '%UEB%'
                and (ht.value != 'AJManufacturing' or hst.host = 'DFWAJMUEB01' or hst.host = 'ATLAJMFUEB02')
            group by u.itemid,ht.value,it.key_,hst.name,it.name,hst.hostid
            order by ht.value,hst.hostid,sku) as t;`,

    inventory: `
        Select Company_ID,qty,ServerName,clock,ItemName,sku,key_ from (
            
            with  
            itmeselect as (
                            Select * from items wi where 
                            wi.name = '/_Stateless: Used space'
                                            or wi.key_  REGEXP  '^vfs.fs.size|NumberOfLogicalProcessors' 
                                        or
                                            wi.key_ = 'vm.memory.size[total]' or 
                                            wi.name = '/_Stateless: Total space' 
            ),
            uint as (
                    SELECT * FROM zabbix.trends_uint u 
                    where 
                    u.clock BETWEEN unix_timestamp(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00')) 
                    AND unix_timestamp(DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59'))
            )
            
            SELECT  
                ht.value as 'Company_ID'
                ,Case
                    when it.key_ like 'system.cpu.num[snmp]' or it.key_ like '%LogicalProc%' then  max(u.value_max)
                else ROUND( (max(u.value_max) /1073741824), 2 )
                END as 'qty'
                ,hst.name as 'ServerName'
                ,from_unixtime(max(u.clock)) as 'clock'
                ,it.name as 'ItemName'
                ,CASE
                when it.key_ like 'vfs.fs.size%' AND it.name like '%Used space' THEN 'CC - Tier 1 Storage'
                when it.key_ like 'vfs.fs%' AND it.name like '%Total space' THEN 'CC - Tier 1  Total Storage'
                when it.key_ like '%hrStorageUse%' then 'CC - Tier 2 Storage'
                when it.key_ like 'vm.memory.size[total%'  THEN 'CC - vMemory'
                else 'CC - vCPU'
                END as sku
                ,it.key_
                
                FROM uint u 
                right join itmeselect it on u.itemid = it.itemid
                join zabbix.hosts hst on  it.hostid = hst.hostid
                join host_tag ht on hst.hostid = ht.hostid
                where 
                    hst.status=0
                    and ht.tag = 'Company'
                group by u.itemid,hst.hostid,ht.value,hst.name,it.name,it.key_
                order by ht.value,hst.hostid,sku) as t

        UNION

        Select 
            ht.value as Company_ID
            ,1 as qty
            ,ho.name as ServerName
            ,now() as clock
            ,hsg.name as ItemName
            ,group_concat(distinct(hsg.name) separator ';') as sku
            ,'N/A' as key_ 
            
            from zabbix.hosts ho
                right join zabbix.host_tag ht on ho.hostid = ht.hostid
                right join zabbix.hosts_groups hg on hg.hostid = ho.hostid
                right join zabbix.hstgrp hsg on hsg.groupid = hg.groupid
            where 
                ho.status = 0 
                 and ht.tag = 'Company'
                and hsg.name in (SELECT name FROM zabbix.hstgrp where name like 'MS -%' or name like 'SECaas%' or name ='Unitrends Backup Appliances') 
            group by ho.hostid,ht.value,ho.name,hsg.name
            order by Company_ID;
    `,


    condenced: `
        Select Company_ID
            , sum(CASE WHEN sku = 'CC - Tier 1  Total Storage' then qty  END) 'CC - Tier 1  Total Storage'
            , sum(CASE WHEN sku = 'CC - Tier 1 Storage' then qty  END) 'CC - Tier 1 Storage'
            , cast(sum(CASE WHEN sku = 'CC - vCPU' then qty END) as signed) 'CC - vCPU'
            , cast(sum(CASE WHEN sku = 'CC - vMemory' then qty  END) as signed) 'CC - vMemory'
            , sum(CASE WHEN sku = 'CC - Tier 2 Storage'  then qty END) 'CC - Tier 2 Storage'
            , cast(sum(CASE WHEN sku = 'MS - Windows Server'  then qty END) as signed) 'MS - Windows Server'
            , cast(sum(CASE WHEN sku = 'Unitrends Backup Appliances' then  qty END) as signed) 'Unitrends Backup Appliances'
            , cast(sum(CASE WHEN sku = 'MS - Hypervisor'  then qty  END) as signed) 'MS - Hypervisor'
            ,cast(sum(CASE WHEN sku = 'MS - Citrix Master HSD' then qty END) as signed) 	'MS - Citrix Master HSD'
            ,cast(sum(CASE WHEN sku = 'MS - Citrix Master VDI' then qty END) as signed) 	'MS - Citrix Master VDI'
            ,cast(sum(CASE WHEN sku = 'MS - Citrix Infra' then qty END) as signed) 	'MS - Citrix Infra'
            ,cast(sum(CASE WHEN sku = 'MS - Citrix Hybrid Infra' then qty END) as signed) 	'MS - Citrix Hybrid Infra'
            ,cast(sum(CASE WHEN sku = 'MS - Citrix Cloud Infra' then qty END) as signed) 	'MS - Citrix Cloud Infra'
            from (
             Select Company_ID,qty,ServerName,clock,ItemName,sku,key_ from (

            with
            itmeselect as (
                            Select * from items wi where
                            wi.name = '/_Stateless: Used space'
                                            or wi.key_  REGEXP  '^vfs.fs.size|NumberOfLogicalProcessors'
                                        or
                                            wi.key_ = 'vm.memory.size[total]' or
                                            wi.name = '/_Stateless: Total space'
            ),
            uint as (
                    SELECT * FROM zabbix.trends_uint u
                    where
                    u.clock BETWEEN unix_timestamp(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00'))
                    AND unix_timestamp(DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59'))
            )

            SELECT
                ht.value as 'Company_ID'
                ,Case
                    when it.key_ like 'system.cpu.num[snmp]' or it.key_ like '%LogicalProc%' then  max(u.value_max)
                else ROUND( (max(u.value_max) /1073741824), 2 )
                END as 'qty'
                ,hst.name as 'ServerName'
                ,from_unixtime(max(u.clock)) as 'clock'
                ,it.name as 'ItemName'
                ,CASE
                when it.key_ like 'vfs.fs.size%' AND it.name like '%Used space' THEN 'CC - Tier 1 Storage'
                when it.key_ like 'vfs.fs%' AND it.name like '%Total space' THEN 'CC - Tier 1  Total Storage'
                when it.key_ like '%hrStorageUse%' then 'CC - Tier 2 Storage'
                when it.key_ like 'vm.memory.size[total%'  THEN 'CC - vMemory'
                else 'CC - vCPU'
                END as sku
                ,it.key_

                FROM uint u
                right join itmeselect it on u.itemid = it.itemid
                join zabbix.hosts hst on  it.hostid = hst.hostid
                join host_tag ht on hst.hostid = ht.hostid
                where
                    hst.proxy_hostid in (select hosts.hostid from zabbix.hosts where hosts.host like 'DFW%ZABBIX01' or hosts.host like 'ATL%ZABBIX01')
                    and hst.status=0
                    and ht.tag = 'Company'
                    and (ht.value != 'AJManufacturing' or hst.host = 'DFWAJMUEB01')
                group by u.itemid
                order by ht.value,hst.hostid,sku) as t

        UNION

        Select
            ht.value as Company_ID
            ,1 as qty
            ,ho.name as ServerName
            ,now() as clock
            ,hsg.name as ItemName
            ,group_concat(distinct(hsg.name) separator ';') as sku
            ,'N/A' as key_

            from zabbix.hosts ho
                right join zabbix.host_tag ht on ho.hostid = ht.hostid
                right join zabbix.hosts_groups hg on hg.hostid = ho.hostid
                right join zabbix.hstgrp hsg on hsg.groupid = hg.groupid
            where
                ho.status = 0
                and ho.proxy_hostid in (SELECT hostid FROM zabbix.hosts where status=5)
                and ht.tag = 'Company'
                and hsg.name in (SELECT name FROM zabbix.hstgrp where name like 'MS -%' or name like 'SECaas%' or name ='Unitrends Backup Appliances') 
            group by ho.hostid
            order by Company_ID) as t
            group by Company_ID;
        `,

    problems: `
    SELECT 
    max(ht.value) as Company_ID,
    from_unixtime(e.clock) as date_time,
    e.severity as epriority
    ,t.priority,
    i.status,
    e.eventid,
    t.triggerid,
    max(h.name) as host_name,
    max(h.hostid),
    max(i.itemid),
    max(i.name) as item_name,
    p.name problem_name,
    p.acknowledged
    FROM zabbix.problem p				
                    join events e on p.eventid = e.eventid
                    join triggers t on t.triggerid=p.objectid
                    join functions f on f.triggerid = e.objectid
                    join items i on i.itemid = f.itemid
                    join hosts h on h.hostid = i.hostid
                    join host_tag ht on h.hostid = ht.hostid
    where
                    e.source = 0 and
                    t.value = 1 and
                    e.severity>=3 and 
                    t.status=0 and
                    r_clock = '' and
                    ht.tag = 'Company' and
                    i.status = 0             
    group by t.triggerid,e.clock,e.severity,zabbix.e.eventid,host
    order by t.priority desc,e.clock desc;`,

    probManagement: `
        SELECT ht.value as Company_ID,t.triggerid,e.eventid,p.name as Problem,h.name as HostName,count(e.eventid) as count FROM zabbix.events e
            join triggers t on t.triggerid=e.objectid
            join problem p on p.eventid = e.eventid
            join functions f on f.triggerid = t.triggerid
            join items i on i.itemid = f.itemid
            join hosts h on h.hostid=i.hostid
            join host_tag ht on h.hostid = ht.hostid
            where e.source = 0 and
            e.clock < date_sub(NOW(), INTERVAL 31 DAY) and
            t.value = 1 and
            t.priority>=3 and 
            h.status = 0 and
            i.status = 0 and
            ht.tag = 'Company'
            group by t.triggerid,ht.value,e.eventid,p.name,h.name
            order by count desc;`,

    problemsCount: `
        Select priority,PName,count(PName) as count from (
        SELECT e.severity as priority,
        CASE
                When e.severity = 5 then 'Disaster'
                WHEN e.severity = 4 then 'High'
                When e.severity = 3 then 'Average'
                When e.severity = 2 then 'Warning'
                ELSE 'Information'
            END as PName,
        
        
        count(t.priority) as count
        FROM zabbix.events e
                join triggers t on t.triggerid=e.objectid
                join problem p on p.eventid = e.eventid
                join functions f on f.triggerid = t.triggerid
                join items i on i.itemid = f.itemid
                join hosts h on h.hostid=i.hostid
                where e.source = 0 and
                t.value = 1 and
                e.severity>=3 and
                h.status = 0 and
                t.status=0 and
                r_clock = '' and
                maintenance_status = 0 and
                i.status = 0
                group by t.triggerid, t.priority,e.severity
                order by e.severity ASC ) T
            group by PName,T.priority
            order by priority desc;`,
    kcuc_wifi_client: `select Mac, 
            max(case when ItemName = 'ClientName' then value else '' end )as 'ClientName',
            max(case when ItemName = 'ClientOperatingSystem'then value else '' end) as 'ClientOperatingSystem',
            max(case when ItemName = 'ClientIPAddress' then value else '' end) as 'ClientIPAddress',
            max(from_unixtime(time)) as 'lastConnected'
        
        from
     
            (select 
                max(ns),
                ht.clock as time,
                ht.value,
                substring_index(items.name, 'ai', -1) as ItemName,
                substring_index(substring_index(items.name, 'ai', 1), "t", -1)  as Mac,
                ht.itemid 
            from history_text ht
            join items on ht.itemid = items.itemid
                where ht.itemid in (SELECT itemid from zabbix.items where key_ like 'ai.client%')
                group by ht.value,items.name,ht.itemid,ht.clock) t
        group by Mac`

}

//Exports 

module.exports = {
    zmConfig,
    zqueries,
    zabbixLogin,
    billHistory
}
