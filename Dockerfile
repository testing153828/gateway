# Use the official Node.js 16 as a parent image
#FROM node:20.11.1

# Set the working directory in the container
#WORKDIR /usr/src/app

# Copy package.json and package-lock.json (if available) to the working directory
#COPY package.json ./
#COPY package-lock.json* ./

# Install any dependencies
#RUN npm install

# Copy the rest of your application's code
#COPY . .

# Your application binds to port 5000, make sure the container does too
#EXPOSE 5000

# Define the command to run your app using CMD which defines your runtime
#CMD [ "npm", "start" ]
# Use an official Node.js base image with your desired version
FROM node:18-alpine

# Install PM2 globally
RUN npm install pm2 -g

WORKDIR /app
COPY package.json .
COPY package-lock.json .

# Install your npm dependencies
RUN npm install

# Copy your application files
COPY . /app/

EXPOSE 5002

# Use PM2 to start your application
CMD ["pm2-runtime", "start", "index.js"]

