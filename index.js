#!/usr/bin/env node
require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const RequestIp = require('@supercharge/request-ip')
var fs = require('fs')
var path = require('path')
const os = require('os')
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./routes/docs/swagger-output.json');
const PORT = process.env.PORT || 5002;
var cors = require('cors')
//const madge = require('madge');

// create a rotating write stream
//var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
var loadaverage = os.loadavg();
  // setup the logger
//app.use(morgan('combined'))
app.use(morgan(function (tokens, req, res) {
    return [
      tokens.method(req, res),
      tokens.url(req, res),
      tokens.status(req, res),
      tokens.res(req, res, 'content-length'), '-',
      tokens['response-time'](req, res), 'ms',
      'Reqest IP: ' + RequestIp.getClientIp(req),
      'Server Load Ave: ' + loadaverage
    ].join(' ')
  })) 
  app.use(morgan('Status -> :status URL -> :url ResponseTime -> :response-time[digits] Method -> :method ReqHeader -> :req[header]'))

app.use(express.urlencoded({ extended: true }))
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));



app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', '*');
    next();
});




app.get("/", cors(), async (req, res) => {

    
    res.send({
        status: "Choice Rest API Server! Ready to Roll!",
    //    cache: myCache.get("myKey"),
    });
});

app.post("/", cors(), async (req, res) => {

    res.send({
        status: "Choice Rest! Ready to Roll!",
        //    cache: myCache.get("myKey"),
    });
});

app.use("/zabbix", require('./routes/api/zabbix'));
app.use("/automate", require('./routes/api/cwautomate'));
app.use("/cwapi", cors(), require('./routes/api/cwmanage'));
app.use("/citrix", require('./routes/api/citrix'));
app.use("/xdr", cors(), require('./routes/api/paloalto'));
app.use("/huntress", cors(), require('./routes/api/huntress'));
app.use("/perch", cors(),  require('./routes/api/perch'));
//app.use("/billUpdate", cors(), require('./routes/api/billingUpdate'));
//app.use("/unitrends", cors(), require('./routes/api/unitrends'));


const server = app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

/**
* @openapi
* components:
*  schemas:
* definitions:
*   cwHash:
*       type: "object"
*       properties:
*       summary:
*           type: "string"
*           description: "Summary field"
*           example: "[External] Alert Email Digest: DRB-Nutanix has 5 unresolved alerts"
*       hash:
*           type: "integer"
*           description: "How many time the summary has appeard"
*           format: "int64"
*       Last Occur:
*           type: "string"
*           format: "date-time"
*           example: "2021-10-21T12:09:03.107Z"
*       xml:
*       name: "cwHash"
*   SLA:
*       type: "object"
*       properties:
*       Ticket Num:
*           type: "integer"
*           description: "Ticket Number"
*           format: "int64"
*       Resolve By:
*           type: "string"
*           format: "date-time"
*           example: "2021-10-21T12:09:03.107Z"
*       Hours Left:
*           type: "integer"
*           description: "How how much time past Resolve By"
*           format: "int64"
*       Company:
*           type: "string"
*           description: "Company Name"
*           example: "Acme Heath"
*       Summary Description
*           type: "string"
*           description: "Summary of Ticket"
*           example: "[External] Alert Email Digest: DRB-Nutanix has 5 unresolved alerts"
*       Service Board
*           type: "string"
*           description: "Service Board"
*           example: "NOC"
*       Status
*           type: "string"
*           description: "Status Field"
*           example: "In Progress"
*       Ticket Owner First Name
*           type: "string"
*           description: "Ticekt Owner First Name"
*           example: "Santiago"
*       Ticket Owner Last Name
*           type: "string"
*           description: "Ticekt Owner Last Name"
*           example: "Ibrahim"
*       xml:
*       name: "SLA"
*   SLACount:
*       type: "object"
*       properties:
*       Count(Ticket Num):
*           type: "integer"
*           description: "Count of Tickets SLA"
*           format: "int64"
*           example: "Ibrahim"
*       xml:
*       name: "SLACount"
*   citrix:
*       type: "object"
*       properties:
*       customerId:
*           type: "string"
*           description: "Citrix Customer ID"
*           example: "dkkeig93ksk"
*       orgid:
*           type: "integer"
*           description: "Citrix Customer Org ID
*           format: "int64"
*           example: "39293333"
*       displayName:
*           type: "string"
*           description: "ChocieCloud"
*           example: "dkkeig93ksk"
*       editionName:
*           type: "string"
*           description: "Virtual Apps Service"
*           example: "dkkeig93ksk"
*       totalCommitCount:
*           type: "integer"
*           description: "Total Purchased (commited lic)""
*           format: "int64"
*           example: "1"
*       totalUsageCount:
*           type: "integer"
*           description: "Total Used"
*           format: "int64"
*           example: "668"
*       totalOverageCount:
*           type: "integer"
*           description: "Total Commit 1 Total Used"
*           format: "int64"
*           example: "667"
*       totalUsagePercent:
*           type: "integer"
*           description: "Total Used Percentage"
*           format: "int64"
*           example: "66800"
*       xml:
*       name: "citrix"
*   condenced:
*       type: "object"
*       properties:
*       customerId:
*           type: "string"
*           description: "Citrix Customer ID"
*           example: "dkkeig93ksk"
*       orgid:
*           type: "integer"
*           description: "Citrix Customer Org ID
*           format: "int64"
*           example: "39293333"
*       xml:
*           name: "condenced"
*   endpoints:
*       type: "object"
*       properties:
*       endpoint_id:
*           type: "string"
*           format: "byte"
*           description: "Cortex Endpoint ID"
*           example: "f0470ca1a6094da59c1236630f6d595a"
*       endpoint_name:
*           type: "string"
*           format: "hostname"
*           description: "Endpoint Name"
*           example: "Workstation_Name"
*       endpoint_status:
*           type: "string"
*           description: "Status of Endpoint"
*           example: "CONNECTED"
*       os_type: 
*           type: "string"
*           description: "OS Descriptioin"
*           example: "AGENT_OS_WINDOWS"
*       os_version: 
*           type: "string"
*           description: "OS Version"
*           example: "10.0.17763"
*       ip: 
*           type: "array"
*           items:
*               type: "string"
*               format: "ipv4"
*               example: ["10.0.1.1", "10.2.3.1"]
*       ip: 
*           type: "array"
*           description: "users"
*           items:
*               type: "string"
*               format: "username"
*               example: ["User1", "User2"]
*       domain: 
*           type: "string"
*           description: "Network Domain Name"
*           example: "domain.local"
*       alias: 
*       is_isolated: 
*           type: "string"
*           description: "Is Device Isolated"
*           description: "No Clue what this is for LOL"
*       isolated_date: 
*           example: "don't have one :)"
*           format: "unix-timestamp"
*       first_seen: 
*           example: 1595901556320
*           type: "integer"
*           type: "array"
*           format: "unix-timestamp"
*           items:
*           description: "Timestamp when first seen"
*               format: "string"
*           example: 1595901556320
*       operational_status: 
*       last_seen: 
*           description: "Operating Status"
*           type: "integer"
*       operational_status_description: 
*           format: "unix-timestamp"
*           description: "Operational Status"
*           description: "Timestamp when last seen"
*               type: "string"
*           example: 1595901556320
*       scan_status: 
*       content_version: 
*           description: "Scan Status"
*           type: "String"
*       last_content_update_time: 
*           description: "Agent Content Version"
*           format: "unix-timestamp"
*           example: "370-82203"
*           example: 1595901556320
*       installation_package: 
*           name: "coretexendpoints"
*           type: "String"
*           description: "Agent Package Version"
*           example: "Windowx64v506"
*       active_directory: 
*           type: "string"
*           description:
*           example: null
*       install_date: 
*           type: "integer"
*           format: "unix-timestamp"
*           description: "Timestamp when device was isolated"
*           example: 1595901556320
*       endpoint_version: 
*           type: "string"
*           description: : "Endping Version"
*           example:  "7.3.1.20981"
*           type: "string"
*           example: "AGENT_UNISOLATED"
*           type: "integer"
*           description: "Timestamp when devices was isolated"
*       group_name: 
*           description: "Device Groups"
*               type: "string"
*               example: ["AJManufacturing"]
*           type: "string"
*           example: "PROTECTED"
*           type: "array"
*           items:
*               example: []
*           type: "string"
*           example: "SCAN_STATUS_NONE"
*           type: "integer"
*           description: "Last content update status"
*       xml:
*            name: "endpoints"
*   zsku:
*       type: "object"
*       properties:
*       Company_ID:
*           type: "string"
*           description: "Zabbix Customer ID Tag"
*           example: "dkkeig93ksk"
*       qty:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       ServerName:
*           type: "string"
*           description: "Device Name"
*           example: "ACEFILE01"
*       clock:
*           type: "string"
*           format: "date-time"
*           descriptions: "Last Time Device Item was updated"
*           example: "2021-10-01T04:00:00.000Z"
*       ItemName:
*           type: "string"
*           description: "Item Name"
*           example: "F:: Total space"
*       sku:
*           type: "string"
*           description: "CW Sku"
*           example: "CC - Tier 1  Total Storage"
*       key_:
*           type: "string"
*           description: "Zabbix Item Key"
*           example: "vfs.fs.size[F:,total]"
*       xml:
*           name: "zsku"
*   condenced:
*       type: "object"
*       properties:
*       Company_ID:
*           type: "string"
*           description: "Company ID"
*           example: "Acme"
*       CC - Tier 1  Total Storage:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       CC - Tier 1 Storage:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       CC - vCPU:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       CC - vMemory:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       CC - Tier 2 Storage:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       MS - Windows Server:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       Unitrends Backup Appliances:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       MS - Hypervisor:
*           type: "integer"
*           description: "Quantity of sku"
*           format: "int64"
*           example: "400"
*       xml:
*           name: "condenced"
    full: 
        type: object
        properties:
        Company_ID:
            type: "string"
            description: "Company ID"
            example: "ChoiceSolutions"
        qty:
            type: "number"
            format: "double"
            description:  "quantatity"
            example: "393.39"
        ServerName: 
            type: "string"
            description: "Device Name"
            example: "DFWCCDC01"
        clock:
            type: "string"
            format: "date-time"
            example: "2022-02-01T05:00:00.000Z"
        ItemName:
            type: "string"
            description: "Name of measured Item"
            example: "2022-02-01T05:00:00.000Z"
        sku:
            type: "string"
            description: "CW SKU Name"
            example: "CC - Tier 1  Total Storage"
        key_:
            type: "string"
            description: "Zabbix key name for item"
            example: "vfs.fs.size[C:,total]"


*/
