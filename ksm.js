const { getSecrets, initializeStorage, localConfigStorage } = require('@keeper-security/secrets-manager-core')

const getKeeperRecords = async () => {

    // oneTimeToken is used only once to initialize the storage
    // after the first run, subsequent calls will use ksm-config.txt
    const oneTimeToken = "-otQiIr_Wjj67R8Ch06AvJD0O8sIe8UdhKNOyQ-rdT0jAJWahK3A_89XsfqZnTlqgguG588MI-7UOuP2IEli4KYEOKVEa9Xx7NiWhJci82fmvjKQ7P87fsBRAfmcnmPs796QWnji6Sv2F6WpFGEExObeYvKzj5UJ-ApQmlY7xXZIa3uDC9edqSZB1jW8LXjlqbYPFWL_juBAWu-Ct9iGbLXY6ZQ0Dsk-j-rHxrsR0I7wLVNQSJNL6i0jUQZxaNC5sMWpkNegJFQ_dH6O2JYhC6YxJhnl2WTbwqRVpulCF_FtoVdtibNMpGEMS7zTuAciUdpFmjemGan59T1si046c4wDubKGT5bGrQk1-sft3Y-5ChLT6UcvIht-d7fHXjGmbEItZLC6IVVXtSVovQyHNnZc37ty9uJWKmI7XvT5EKNfXnc2HqofsAorMlXRHyu4PnkKBtOXHAR4VV4_9d8lmzpRtajjYVMCr7Zelk4_rDRGKxNkPxNpomJv7s-G6SujpxTuxbIqRrsNRr4sm3cXLw8hzPKsmg";
    
    const storage = localConfigStorage("ksm-config.json")
    
    await initializeStorage(storage, oneTimeToken)
    // Using token only to generate a config (for later usage)
    // requires at least one access operation to bind the token
    //await getSecrets({storage: storage})
    
    const {records} = await getSecrets({storage: storage})
    console.log(records)

    const firstRecord = records[0]
    const firstRecordPassword = firstRecord.data.fields.find(x => x.type === 'password')
    console.log(firstRecordPassword.value[0])
}

getKeeperRecords().finally()