// const { Router } = require('express');
const express = require('express')
const cwarouter = express.Router()
const mysql = require("mysql");
// const axios = require('axios');
// //const automate = require('./cwautomate');

const {
    queries,
    cwAutomateConfig 

 } = require("../../configs/cwAutomate");
cwarouter.get("/", (req, res) => {
    res.send("Hello Automate")
})

const {
    globalGatewayCache
} = require("../../configs/cache.js");

 cwarouter.get("/sql/:sql", async (req, res) => {

    if(globalGatewayCache.has("automate_"+req.params.sql)){
        console.log("Returning Automate " + "automate_"+req.params.sql + " from Cache")
        res.json(globalGatewayCache.get("automate_"+req.params.sql))
    }
    /* #swagger.description = 'Connectwise Automate utilizing back end sql queries instead of Connectwise REST calls<br>
       &emsp;<b>workstations:</b> List of Workstations and Servers <br>
       &emsp;<b>patchRebootAudit:</b> Patch Reboot Alert <br>
       &emsp;<b>patchAudit:</b> Patch Audit Report <br>
       &emsp;<b>patchfails:</b> Patch Failed Report <br>
       &emsp;<b>activeDirUsers:</b> AD user report <br>
       &emsp;<b>spla:</b> Microsoft SPLA report <br>
       &emsp;<b>badHealth:</b> Connectwise Bad Device Health Report <br>
       '

       #swagger.produces = ["application/json"]
        #swagger.responses[200] = {
            description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
            schema: {
                oneOf:[
                    {
                        $ref: "#/definitions/workstations",
                    },
                    {
                        $ref: "#/definitions/patchRebootAudit",
                    },
                    {
                        $ref: "#/definitions/patchAudit",
                    },
                    {
                        $ref: "#/definitions/activeDirUsers",
                    },
                    {
                        $ref: "#/definitions/spla",
                    },
                    {
                        $ref: "#/definitions/badHealth",
                    }
                ]
            }
         } 
    */
    else{
        console.log("NOT Returning Automate " + "automate_"+req.params.sql + " from Cache")
        const pool = mysql.createPool(cwAutomateConfig);
        if (queries[req.params.sql]) {
            pool.query(queries[req.params.sql], (error, results) => {
                if (!results) {
                    res.json({ status: "Not found! " + error });
                } else {
                    
                    if(req.params.sql == 'computers')
                    {
                        let temp = {};
                        let companyRec = {};
                        if(globalGatewayCache.has("device2Company")){
                            companyRec = globalGatewayCache.get("device2Company");
                        }

                        for(let i=0; i<results.length; i++){
                            if(typeof results[i].IsServer !== 'undefined'){
                            let k = results[i].computerName;

                            if(typeof companyRec[results[i].computerName] != "undefined"){
                                companyRec[results[i].computerName]['company_RecID']= results[i].Company_RecID 
                            }else{companyRec[results[i].computerName] = {'company_RecID': results[i].Company_RecID };}

                        }}
                        globalGatewayCache.set("device2Company", companyRec);

                    }
                    globalGatewayCache.set("automate_"+req.params.sql, results)
                    res.json(globalGatewayCache.get("automate_"+req.params.sql));
                }
            })
        } else res.send('no params *' + req.params.sql + '*');
    }
});


cwarouter.get("/sqltable/:sql", async (req, res) => {
    /* #swagger.description = 'Connectwise Automate utilizing back end sql queries instead of Connectwise REST calls Deleiverd as a Table<br>
       &emsp;<b>workstations:</b> List of Workstations and Servers <br>
       &emsp;<b>patchRebootAudit:</b> Patch Reboot Alert <br>
       &emsp;<b>patchAudit:</b> Patch Audit Report <br>
       &emsp;<b>patchfails:</b> Patch Failed Report <br>
       &emsp;<b>activeDirUsers:</b> AD user report <br>
       &emsp;<b>spla:</b> Microsoft SPLA report <br>
       &emsp;<b>badHealth:</b> Connectwise Bad Device Health Report <br>
       '

       #swagger.produces = ["application/json"]
        #swagger.responses[200] = {
            description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
            schema: {
                oneOf:[
                    {
                        $ref: "#/definitions/workstations",
                    },
                    {
                        $ref: "#/definitions/patchRebootAudit",
                    },
                    {
                        $ref: "#/definitions/patchAudit",
                    },
                    {
                        $ref: "#/definitions/activeDirUsers",
                    },
                    {
                        $ref: "#/definitions/spla",
                    },
                    {
                        $ref: "#/definitions/badHealth",
                    }
                ]
            }
         } 
    */
    const flatten = require('flat').flatten;
    const row = html => `<tr>\n${html}</tr>\n`,
        heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
        datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));
    const pool = mysql.createPool(cwAutomateConfig);

    if (queries[req.params.sql]) {
        pool.query(queries[req.params.sql], (error, results) => {
            if (!results) {
                res.json({ status: error });
            } else {
                //res.json(results);
                res.send(`<table border="1">
         ${heading(results[0])}
          ${results.reduce((html, object) => (html + datarow(flatten(object))), '')}
        </table>`)
            }
        })
    } else res.send('no params *' + req.params.sql + '*');

});



module.exports = cwarouter;