/* TODO *************************************************************************************************
 I forsee that using a foreach on companies will be extremely helpful. Here is sample code:

 const companies = [
    {
        "id": 32123,
        "is_mssp": true,
        "name": "Choice Cloud",
        "display_name": "Choice Cloud"
    },
    {
        "id": 32125,
        "is_mssp": false,
        "name": "Choice Solutions",
        "display_name": "Choice Solutions"
    },
    {
        "id": 35683,
        "is_mssp": false,
        "name": "PDKC Pediatric Dermatology of Kansas City",
        "display_name": "PDKC Pediatric Dermatology of Kansas City"
    },
    {
        "id": 33063,
        "is_mssp": false,
        "name": "Queens Price Chopper",
        "display_name": "Queens Price Chopper"
    },
    {
        "id": 32178,
        "is_mssp": false,
        "name": "Tickets for Less",
        "display_name": "Tickets for Less"
    }
];
companies.forEach(myFunction);

document.getElementById("demo").innerHTML = text;
 
function myFunction(item, index) {
  text += index + ": " + item.name + "<br>"; 
}
</script>

****************************************************************************************************************/

const { Router } = require('express');
const express = require('express');
const perchrouter = express.Router();
const perch = require('./perch');
const axios = require('axios');

const {
     getCompanies
} = require("../../configs/perch.js");


perchrouter.get('/companies', async (req, res) => {
   /* #swagger.description = 'Perch Company List'

       #swagger.produces = ["application/json"]
   */
   let companies = await getCompanies();
   res.send(companies);

 }) 


module.exports = perchrouter;

