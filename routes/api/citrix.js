const { Router } = require('express');
const express = require('express');
const router = express.Router();
const citrix = require('./citrix');
const axios = require('axios');


const {
  transform,transformToZabbixFormat,citrixCloudLic,apiCustBaerer,cust_info
} = require("../../configs/citrixcloud.js");

router.get('/lic/:tenant?', async (req, res) => {
    /* #swagger.description = 'Citrix Cloud Tenant Lic Count<br>
       &emsp;<b>singleTenantsUsage:</b> Singe Tenant Lookup<br>
       &emsp;<b>multiTenantsUsage:</b> Multi Tenant Lookup<br>'
 
      #swagger.produces = ["application/json"]
       #swagger.responses[200] = {
           description: "Returns Licences for tenant",
           schema: {
                    
                        
                        "customerId": "be4258lg1b38",
                        "orgId": "52279586",
                        "displayName": "ChoiceSolutions",
                        "editionName": "Virtual Apps and Desktops Service",
                        "totalCommitCount": 2,
                        "totalUsageCount": 40,
                        "totalOverageCount": 38,
                        "totalUsagePercent": 2000
                        
                    
                   },
           
        }
    */
    let single = {"tenant":[]};
 
    let cvadlic = await citrixCloudLic('cvad');
    //res.send(lic);
    let daaslic = await citrixCloudLic('daas');

    single = transform(single,cvadlic,"singleTenantsUsage");
    single = transform(single,daaslic,"singleTenantsUsage");
    single = transform(single,cvadlic,"multiTenantsUsage")
  //  single = transform(single,daaslic,"multiTenantsUsage")
    let old = await citrixCloudLic('old');

    let tranform_old = {
        "type" : 'onPremise',
        "customerId": old.skuUsages[0].featureUsages[0].linkedCustomerUsages[0].customerId,
        "orgId": old.skuUsages[0].featureUsages[0].linkedCustomerUsages[0].customerOrgId,
        "displayName": old.skuUsages[0].featureUsages[0].linkedCustomerUsages[0].customerNickName,
        "editionName": old["editionName"] || "ROYSPLACXDPREM",
        "totalCommitCount":old["totalCommitCount"] || "N/A",
        "totalUsageCount": old.skuUsages[0].featureUsages[0].count,
        "totalOverageCount": old["totalOverageCount"] || "N/A",
        "totalUsagePercent": old["totalUsagePercent"] || "N/A",
    }

    single["tenant"].push(tranform_old);
  //  single["tenant"].push(cvadlic)
    

    res.json(single.tenant);
   // res.json(old);


})


router.get('/Daas/:customerID/:item/:nameOrId?', async (req, res) => {
  const citrixToken = await apiCustBaerer(req.params.customerID);
  const output = req.get('User-Agent') ? req.get('User-Agent'): 'standard';
  let filter = req.query.filter ? req.query.filter : 'true';

 const baseurl = 'https://api-us.cloud.com/cvad/manage';
 let urls = {
    'DeliveryGroups': {
      params:'/DeliveryGroups',
      qparams: { fields: 'Name,Id,ProvisioningType,SessionSupport,MachineType'  }
    },
    'DeliveryGroup': {
      params:'/DeliveryGroups/' + req.params.nameOrId,
    },  //Query Params fields
    'DeliveryGroupMC': {
      params: '/DeliveryGroups/' + req.params.nameOrId + '/MachineCatalogs'
    },
   // 'DG_MachineCatalogs': {params:'/MachineCatalogs/' + req.params.nameOrId,},//Query Params fields
    'MachineCatalogs': {
        params:'/MachineCatalogs',
        qparams:{
          'fields': 'Id,OSType,Name,IsPowerManaged,ProvisioningType,SessionSupport,IsBroken,IsMasterImageAssociated,IsRemotePC'
        }
      }, // Query Params fields
    'MachineCatalog': {params:'/MachineCatalogs/'+req.params.nameOrId,}, // Query Params fields
    'MachineCatalogMachines': {
      params:'/MachineCatalogs/' + req.params.nameOrId + '/Machines',
      qparams:{
        'fields': 'AgentVersion,InMaintenanceMode,LastConnectionFailure,LastDeregistrationReason,ScheduledReboot,Id,Sid,SummaryState,FaultState,Tags,Name,DeliveryType,Hosting,IPAddress,MaintenanceModeReason',
        
        }},
    'MachineCatalogsDG': {
      params:'/MachineCatalogs/'+req.params.nameOrId,
      qparams:{
        'fields': 'DeliveryGroups',
        'flatten': true
        }
      }, // Query Params fields
    'MachineCatalogMasterImage': {
      params:'/MachineCatalogs/' + req.params.nameOrId + '/LastMasterImage',
    },
    'MachineCatalogMachinesAccounts': {
      params:'/MachineCatalogs/' + req.params.nameOrId + '/MachineAccounts',
    },
    'Machines': {
      params: '/Machines',
    },  //qparams:{'sessionSupport': 'MultiSession','limit': 1,}},// 'fields': 'Uid,AgentVersion,AllocationType'}},
    'Machine': {
      params:'/Machines/' + req.params.nameOrId,
    },
    'MachineDesktop': {
      params:'/Machines/' + req.params.nameOrId + '/Desktop',
    },
    'MachineMachineCatalog': {
      params:'/Machines/'+ req.params.nameOrId  +'/MachineCatalog',
    },
    'MachineSessions': {
      params:'/Machine/'+ req.params.nameOrId + '/Sessions',
    },
    'Sessions': {
      params:'/Sessions',
    },
    'Session': {
      params:'/Sessions/' + req.params.nameOrId,
    },

 }
 url = baseurl+urls[req.params.item]['params'];
 const parameters = urls[req.params.item]['qparams'] ? urls[req.params.item]['qparams']  : '';


 const headers = {
  'Accept' : 'application/json',
  'Citrix-CustomerId': cust_info[req.params.customerID]['customerid'], 
  'Citrix-InstanceId': citrixToken['Customer-Site'],
  'Authorization': 'CwsAuth Bearer=' + citrixToken['token'],
  'Content-Type' : 'application/json',
  'Citrix-Locale': 'en-US'
  };
  try {
    const response = await axios.get(url, { headers: headers });
    console.log('Actual URL Sent:', response.request.res.responseUrl);
    //console.log(response.data);

    let filteredData = response.data.Items ? response.data.Items : response.data;
    filteredData = typeof filteredData === "object" ? filteredData : JSON.parse(filteredData || '{}')    ;
    filteredData = Array.isArray(filteredData) ? filteredData : [filteredData];
    
    //console.log(filteredData);
    if (urls[req.params.item]?.qparams?.fields && filter === 'true') {
       // filteredData = response.data.Items.map(item => {
        filteredData = filteredData.map(item => {
        const filteredItem = {};
        const fields = urls[req.params.item]['qparams'].fields
        const dynamicFields = fields.split(',');
        for (const field of dynamicFields) {
          if (item[field] !== undefined) {
            // Only include the field in filteredItem if it exists in the item
            filteredItem[field] = item[field];
       //     console.log(`${field}: ${item[field]}`);
          }
        }
        if(urls[req.params.item]?.qparams?.flatten  === true)
        {
          console.log('flatten')
          filteredData = filteredData.flatMap(item => item.DeliveryGroups || []);
        }
        return filteredItem;
      }).filter(filteredItem => Object.keys(filteredItem).length > 0);
      //console.log(filteredData);
    }

  
    

      // Handle the API response
      //  if(output==='zabbix'){res.json(transformToZabbixFormat(response.data))}
      if(output==='zabbix'){res.json(transformToZabbixFormat(filteredData))}
      else {//res.json(response.data)
        res.json(filteredData)
      }
   
  }catch(error) {
    // Handle errors
    if (error.response) {
      console.error('Server responded with status', error.response.status);
      res.json(error.response.data);
    } else if (error.request) {
        console.error('No response received from the server');
    } else {
        console.error('Error setting up the request:', error.message);
    }
  };

})

router.get('/monitor/:customerID/:monitorType', async (req, res) =>{
    const output = req.get('User-Agent') ? req.get('User-Agent'): 'standard';
    let url = 'https://api-us.cloud.com/monitorodata'
    let cwCustId = req.params.customerID
    let machineid = req.query.machineid ? req.query.machineid : '';
    let sessionid = req.query.sessionid ? req.query.sessionid : '';
    let catalogid = req.query.catalogid ? req.query.catalogid : '';
    let desktopid = req.query.desktopid ? req.query.desktopid : '';
    const formattedDateThisHour = new Date().setMinutes(0, 0, 0);
    let filters = {
      'Sessions': `/Sessions?$expand=User,Machine,SessionMetrics&$apply=filter(LogOnDuration gt 120000 and StartDate gt 2024-02-09)&$top=5`,
      'Machines': '/Machines?$apply=filter(IPAddress ne null )',
      'Machine':  `/Machines?$apply=filter(Id eq ${machineid})&$expand=Sessions($apply=filter(StartDate eq 2024-02-28)),Catalog($select=Id,Name),DesktopGroup($select=Id,Name,IsRemotePC,DesktopKind,DeliveryType,IsInMaintenanceMode),Hypervisor($select=Id,Name),ProcessInfo,MachineMetric($orderby=CollectedDate desc;$top=1),LoadIndex($orderby=CreatedDate desc;$top=4),ResourceUtilization,ProcessUtilizationHourSummary `,
      'SessionActivitySummaries': '/SessionActivitySummaries',
      'Catalogs': '/Catalogs?$apply=filter(LifecycleState eq 0)',//'',
      'DesktopGroups': '/DesktopGroups?$apply=filter(LifecycleState eq 0)',
      'Connections': '/Connections?$apply=filter(ClientPlatform ne null and LogOnStartDate ge 2024-02-05)&$expand=Session($expand=User,Machine)',
      'ProcessUtilization': '/ProcessUtilization',
      'LogOnMetrics': '/LogOnMetrics?$apply=filter(UserInitStartDate ne null)&$expand=Session($expand=User,Machine)&$top=3',
      'Hypervisors' : '/Hypervisors',
      'TaskLogs' : '/TaskLogs',
      'Users' : 'Users',
      'MachineMetrics': '/MachineMetrics',
      'MachineFailureLogs' : '/MachineFailureLogs?$apply=filter(FailureStartDate ge 2024-02-28)'
    };
 
    apiUrl = url + filters[req.params.monitorType] + '&count=true';
    const citrixToken = await apiCustBaerer(cwCustId);
    const headers = {
      'Accept' : 'application/json',
      'Citrix-CustomerId': cust_info[cwCustId]['customerid'], //"ChoiceSoluti",
      'Citrix-ConnectorId': citrixToken['Customer-Site'],
      'Authorization': 'CwsAuth Bearer=' + citrixToken['token'],
      'Content-Type' : 'application/json'
    };
  axios.get(apiUrl, { headers })
    .then(response => {
    // Handle the API response
    if(output==='zabbix'){res.json(transformToZabbixFormat(response.data.value))}
    else {res.json(response.data.value)}
    })
    .catch(error => {
      // Handle errors
      if (error.response) {
        res.json(error.response.data)
        console.error('Server responded with status', error.response.status);
        console.error('Response data:', error.response.data);
      } else if (error.request) {
          console.error('No response received from the server');
      } else {
          console.error('Error setting up the request:', error.message);
      } });

})


router.get('/adclic', async (req, res) => {
    let adclic = await citrixCloudLic('adc');
    const filteredNetScalerCallingHomeDetails = adclic.netScalerCallingHomeDetails.filter((details) => {
        const licenseModels = details.netScalerLicenseModels;
        return licenseModels.every((model) => model.isReporting !== false  && model.isFoundInBackOffice === true);
      });

    const customerMap = new Map();
    for (const details of filteredNetScalerCallingHomeDetails) {
        for (const customer of details.taggedCustomers) {
            const customerId = customer.customerId;
            const customerDisplayName = customer.customerDisplayName;
            const hosts = customerMap.get(customerId) || new Set();
            hosts.add(details.hostId);
            customerMap.set(customerId, hosts);
        }
    }
    
    // Count the unique host IDs and NetScalers for each customer ID
    const customerCounts = [];
    for (const [customerId, hosts] of customerMap.entries()) {
        const hostCount = hosts.size;
        let netScalerCount = 0;
        let customerDisplayName = '';
        for (const details of filteredNetScalerCallingHomeDetails) {
            if (details.taggedCustomers.some(customer => customer.customerId === customerId)) {
            netScalerCount += details.netScalerLicenseModels.length;
            customerDisplayName = customerDisplayName || details.taggedCustomers.find(customer => customer.customerId === customerId)?.customerDisplayName;
            }
            
        }
        customerCounts.push({ customerId, customerDisplayName, hostCount, netScalerCount });
    }




const customerMap2 = new Map();

for (const details of filteredNetScalerCallingHomeDetails) {
//for (const details of adclic.netScalerCallingHomeDetails) {   

  console.log(details.taggedCustomers[0].customerId);
  const customerId = details.taggedCustomers[0].customerId;
  const customerDisplayName = details.taggedCustomers[0].customerDisplayName;
  const customerOrgId = details.taggedCustomers[0].customerOrgId;
  const customerNickName = details.taggedCustomers[0].customerNickName;
  const hostId = details.hostId;
  const fqdn = details.fqdn;
  const serverGuid = details.serverGuid;

  let netscalerCount = 1;
  let totalLicCount = 0;
  let productName = details.netScalerLicenseModels[0].productName;

  for (const model of details.netScalerLicenseModels) {
    

    if (model.deploymentType !== "HA Secondary") {
      if (totalLicCount === 0) {
        //netscalerCount++;
        totalLicCount++;
        productName = model.productName;
      } else {
        console.log(`Warning: More than one NetScaler license model found for host ${hostId}`);
      }
    }
  }

  const customerDetails = { customerId, hostId, fqdn, serverGuid, customerDisplayName, customerOrgId, customerNickName, netscalerCount, totalLicCount, productName };

  if (customerMap2.has(hostId)) {
    customerMap2.get(hostId).push(customerDetails);
  } else {
    customerMap2.set(hostId, [customerDetails]);
  }
}

const output = [];

for (const [hostId, details] of customerMap2.entries()) {
  let totalNetscalerCount = 0;
  let totalHostCount = 0;
  let productName = "";

  for (const detail of details) {
    totalNetscalerCount += detail.netscalerCount;
    totalHostCount += detail.totalLicCount;
    if (productName === "") {
      productName = detail.productName;
    } else if (productName !== detail.productName) {
      console.log(`Warning: More than one product name found for customer ${customerId}`);
    }
  }

  output.push({ hostId, customerDetails: details, netscalerCount: totalNetscalerCount, totalHostCount, productName });
}


let result = [];
result.push(output);

    res.send(result)

})

router.get('customerlic', async (req, res) => {
  
} ) 

router.get('/licAllow', async (req, res) => {
    let adclic = await citrixCloudLic('lic');
    res.json(adclic)
})

module.exports = router;