const { Router } = require('express');
const express = require('express');
const zrouter = express.Router();
const mysql = require("mysql");
const axios = require('axios');
const Zabbix = require('zabbix-rpc')

const {
    globalGatewayCache
} = require("../../configs/cache.js");


const {
    zqueries,
    zmConfig,
    zabbixLogin,
    billHistory

} = require("../../configs/zabbix");

const z = new Zabbix('https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php');


z.user.login(process.env.ZBX_apiUser, process.env.ZBX_apiPass, )
z.user.check().then(console.log)

const now = new Date();
const currentMonth = now.getMonth() + 1 ; // add 1 to get 1-based month
const firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
const previousMonth = firstDayPrevMonth.getMonth() + 1; // add 1 to get 1-based month
const prevMonthYear = firstDayPrevMonth.getFullYear();
const currentYear = now.getFullYear();

console.log(previousMonth+ '/'+ prevMonthYear+ '*****************************')



function runZabbixQuery(sql, callback){
    if(globalGatewayCache.has('zabbix_'+zqueries[sql])){
        console.log("Returning Zabbix " + 'zabbix_'+zqueries[sql]  + " from Cache");
        callback(globalGatewayCache.get('zabbix_'+zqueries[sql]));
    }
   // console.log(sql);
    const pool = mysql.createConnection(zmConfig);
    pool.connect();
    var companyID = {};
    var getCompanyIDCache = {};
    pool.query(zqueries[sql], (error, results) => {
        if (!results) {
           console.log({ status: error });
            return({ status: error } + sql);
        } else {
            globalGatewayCache.set('zabbix_'+sql, results);   
            callback(results);          
        }
    })

}



zrouter.get("/rpc/hosts", async (req,res) => {
    
    let hosts = await z.host.get();
    res.send(hosts);
})

zrouter.get("/rpc/bandwidthBilling", async (req,res) => {
    let items = await z.itmes.get({
        search: {
            tag: ['customerid', 'internet']
        },
        output: ['itemid', 'lastvalue'],
    })
})



zrouter.get("/billupdate", async (req,res) => {
    // END: ed8c6549bwf9


    const billpool = mysql.createConnection(billHistory);
    let citrix2company = new Map();
    let huntress2company = new Map();
    console.log('billing Update Running');
    
    // Define an async function to handle the asynchronous operations
    async function fetchData() {
      try {
        console.log('*************************************cwCompany');
        const response = await axios.get('http://127.0.0.1:5000/cwapi/queries/cwCompany');
        
    
        // Monitor
        subset = response.data.map((co) => {
          if (co.Userfield_2 && co.Company_RecID) { 
            citrix2company.set(co.Userfield_2,co.Company_RecID);
          } //            
          if (co.Userfield_7 && co.Company_RecID) { 
            huntress2company.set(co.Userfield_7,co.Company_RecID);
          }
          return [co.Company_RecID, co.Company_ID, co.Company_Name, co.Userfield_1, co.Userfield_2, co.Userfield_3, co.Userfield_4, co.Userfield_5, co.Userfield_6, co.Userfield_7, co.Userfield_8, co.Userfield_9, co.Userfield_10];
        });
    
        // Continue with the rest of your code that depends on citrix2company
        const sql = `INSERT IGNORE INTO Company (Company_RecID, Company_ID, Company_Name, Userfield_1, Userfield_2, Userfield_3,Userfield_4, Userfield_5, Userfield_6, Userfield_7, Userfield_8, Userfield_9, Userfield_10) VALUES ? 
          on DUPLICATE KEY UPDATE 
          Userfield_1 = VALUES(Userfield_1),
          Userfield_2 = VALUES(Userfield_2),
          Userfield_3 = VALUES(Userfield_3),
          Userfield_4 = VALUES(Userfield_4),
          Userfield_5 = VALUES(Userfield_5),
          Userfield_6 = VALUES(Userfield_6),
          Userfield_7 = VALUES(Userfield_7),
          Userfield_8 = VALUES(Userfield_8),
          Userfield_9 = VALUES(Userfield_9),
          Userfield_10 = VALUES(Userfield_10);`;
    
   
        // Continue with the rest of your code that depends on the filled huntress2company array
        console.log('*************************************Huntress');
        axios.get('http://127.0.0.1:5000/huntress/orgs')
        .then(response => {
            console.log('Huntress is being fetched')
            subset = response.data.map((huntress) => {
                return [huntress2company.get(huntress.id.toString()),huntress.id, huntress.agents_count,huntress.name, huntress.sku, prevMonthYear, previousMonth, huntress.key, 'Huntress'];
            })
            const sql = 'INSERT IGNORE INTO monitoring_history ( Company_RecID, Monitor_Company_ID, Qty, device, Sku, year, month, ItemName, Monitor) VALUES ? on DUPLICATE KEY UPDATE Company_RecID = VALUES(Company_RecID)';
            console.log(sql);

            billpool.query(sql, [subset], (err, result) => {
                if (err) throw err;
                message ='New record added with ID:' + result.insertId;
                console.log(message);
            })
        })
        .catch(error => {
            console.log(error);
        });
        console.log('*************************************Citrix');
        axios.get('http://10.242.157.160:5000/citrix/lic/ek')
        .then(response => {
            let citrixlic =  new Map();
            
            citrixlic.set('A.J. Manufacturing Company, Inc.',4070426);
            citrixlic.set('ACE - Auditing, Compliance, & Education',4082413);
            citrixlic.set('B',4067150)
            citrixlic.set('Anytime Bail Bonding',4082413);
            citrixlic.set('Avyve',4067160);
            citrixlic.set('Central States Group',4067150);
            citrixlic.set('ChoiceDev',4067160);
            citrixlic.set('ChoiceSolutions',4067160);
            citrixlic.set('Clubessential',4082413);
            citrixlic.set('MSI Mold Builders',4082413);
            citrixlic.set('Opticall',4070577);
            citrixlic.set('Pediatric Dermatology of Kansas City',4067150);
            citrixlic.set('SoleoHealth',4084650);
            citrixlic.set('Venture Construction', 4082413);
            citrixlic.set('RSI',4084650);
            citrixlic.set('Tickets For Less',4082413);
            

            subset = response.data
              .filter(citrix => citrix.displayName !== 'NRG Global')
              .map(citrix => {
                 return [citrix2company.get(citrix.orgId.toString()),citrix.orgId, citrix.totalUsageCount,citrix.displayName, citrixlic.get(citrix.displayName), prevMonthYear, previousMonth, citrix.editionName, 'Citrix'];
              })
 
            const sql = 'INSERT INTO monitoring_history (Company_RecID, Monitor_Company_ID, Qty, device, Sku, year, month, ItemName, Monitor) VALUES ? ON DUPLICATE KEY UPDATE Company_RecID = Values(Company_RecID), Qty = VALUES(Qty), device = VALUES(device), Sku = VALUES(Sku), ItemName = VALUES(ItemName), Monitor = VALUES(Monitor)';
            const values = subset.map(row => Object.values(row));
            const formattedSql = billpool.format(sql, [values]);
            console.log(formattedSql)
            

            billpool.query(formattedSql, (err, result) => {
                if (err) throw err;
                if (result.affectedRows > 0) {
                    message = 'Record updated for keys monitore_company_Id: ' + subset[0][1] + ', month: ' + subset[0][6] + ', year: ' + subset[0][5];
                } else {
                    message = 'New record added with ID: ' + result.insertId;
                }
                console.log('Huntress &&&&&&&&&&&&&&&&&&&&&&&&&&&&&' + result);
            });
        })
        .catch(error => {
            console.log(error);
        });

        // axios.get('http://10.242.157.160:5000/citrix/adclic')
        // .then(response => {

        //         let subset = response.data[0].map((adc) => {
        //             return [
        //             citrix2company.get(adc.customerDetails[0].customerOrgId.toString()),
        //             adc.customerDetails[0].customerOrgId,
        //             adc.customerDetails[0].totalLicCount,
        //             adc.customerDetails[0].fqdn,
        //             adc.customerDetails[0].productName,
        //             prevMonthYear,
        //             previousMonth,
        //             adc.hostId,
        //             'Citrix'
        //             ];
        //         })
              
        //     //console.log('ADC: ' + subset);
        //     const sql = 'INSERT INTO monitoring_history (Company_RecID, Monitor_Company_ID, Qty, device, Sku, year, month, ItemName, Monitor) VALUES ? ON DUPLICATE KEY UPDATE Company_RecID = Values(Company_RecID), Monitor_Company_ID = VALUES(Monitor_Company_ID), Qty = VALUES(Qty), device = VALUES(device), Sku = VALUES(Sku), ItemName = VALUES(ItemName), Monitor = VALUES(Monitor)';
        //     const values = subset.map(row => Object.values(row));
        //     const formattedSql = billpool.format(sql, [values]);
        //     billpool.query(formattedSql, (err, result) => {
        //         if (err) throw err;
        //         if (result.affectedRows > 0) {
        //             message = 'Record updated for keys monitore_company_Id: ' + subset[0][1] + ', month: ' + subset[0][6] + ', year: ' + subset[0][5];
        //         } else {
        //             message = 'New record added with ID: ' + result.insertId;
        //         }
        //      //   console.log(message);
        //     });
        // })
        // .catch(error => {
        //     console.log(error);
        // });

        await billpool.query(sql, [subset]);
        console.log('Query executed successfully');
 

      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }

    // Call the async function to initiate the fetching and processing of data
    fetchData();

    runZabbixQuery('full',(resJson) => {
        let message = ""
        let subset = resJson.map((sku) => {
            return [sku.Company_ID, sku.qty, sku.ServerName,sku.sku, prevMonthYear,previousMonth, sku.ItemName, 'Zabbix'];
          });
            const sql = 'INSERT IGNORE INTO monitoring_history (Company_ID, qty, device, Sku, year, month, ItemName, Monitor) VALUES ?';
            billpool.query(sql, [subset], (err, result) => {
            if (err) throw err;
            message ='New record added with ID:' + result.insertId;
            });

        })

        axios.get('http://127.0.0.1:5000/automate/sql/spla')
            .then(response => {
                console.log('Ran Spla')
            //Monitor
            subset = response.data.map((sku) => {
                return [sku.clientID, sku.clientName, sku.Company_RecID, sku.processors/2, sku.name,sku.sku, prevMonthYear,previousMonth, sku.ItemName, 'Automate'];
              });
              const sql = 'INSERT IGNORE INTO monitoring_history (Monitor_Company_ID, Company_Name, Company_RecID, Qty, device, Sku, year, month, ItemName, Monitor) VALUES ?';
              billpool.query(sql, [subset], (err, result) => {
              if (err) throw err;
              message ='New record added with ID:' + result.insertId;
                })
            })
            .catch(error => {
            console.log(error);
            });

        axios.get('http://127.0.0.1:5000/automate/sql/computers')
            .then(response => {
                console.log('Ran Automate Computers')
            //Monitor
                subset = response.data.map((sku) => {
                    return [sku.clientID, sku.clientName, sku.Company_RecID, 1, sku.computerName ,sku.sku, prevMonthYear,previousMonth, sku.ItemName, 'Automate'];
                });
                const sql = 'INSERT IGNORE INTO monitoring_history (Monitor_Company_ID, Company_Name, Company_RecID, Qty, device, Sku, year, month, ItemName, Monitor) VALUES ?';
                billpool.query(sql, [subset], (err, result) => {
                    if (err) throw err;
                    message ='New record added with ID:' + result.insertId;
                })
            })
            .catch(error => {
                console.log(error);
            });
    

    billpool.query(zqueries.billtableQC, (err, result) => {
        if(err) throw err;
        let message = '############################################################################monitortable QCd :' + result.insertId;
        console.log(message)

    })
        

        res.send('Complete');

    
 })

zrouter.get("/billSql/:sql", async (req,res) => {

    const billconnection = mysql.createConnection(billHistory);
   console.log(zqueries[req.params.sql]);
    billconnection.query(zqueries[req.params.sql], (err, result) => {
        if (err) throw err;
        res.send(result);
        });

})

zrouter.get("/sql/:sql", async (req, res) => {

    if(globalGatewayCache.has('zabbix_'+req.params.sql)){
        console.log("Returning Zabbix " + 'zabbix_'+req.params.sql  + " from Cache");
        res.json(globalGatewayCache.get('zabbix_'+req.params.sql));
    }
    /* #swagger.description = 'Zabbix  urilizing back end sql queries instead of Zabbix REST calls: <br>
        &emsp;<b>full:</b> Zabbix SKU search results <br>
        &emsp;<b>condenced:</b> Sku Condenced <br>
        &emsp;<b>probManagement:</b> Top Problems from the last 30 days <br>
        &emsp;<b>problems:</b> Problems in Zabbix  <br>
        &emsp;<b>problemsCount:</b> Count of problems grouped by severity <br>
    '

    #swagger.produces = ["application/json"]
    #swagger.responses[200] = {
        description: 'Zabbix  urilizing back end sql queries instead of Zabbix REST calls',
        schema:{
            anyOf: [
                {
                    $ref: "#/definitions/full",
                },
                {
                    $ref: "#/definitions/condenced"
                },
                {
                    $ref: "#/definitions/problemManagment"
                },
                {
                    $ref: "#/definitions/problems"
                },
                {
                    $ref: "#/definitions/problemsCount"
                },
            ]
        }
    } 
    */

    else {
        
        console.log("Not Returning Zabbix " + 'zabbix_'+req.params.sql  + " from Cache");
        if (zqueries[req.params.sql]) {
            runZabbixQuery(req.params.sql,(resJson) => {
                res.send(resJson);
              })         
        } else res.send('no params *' + req.params.sql + '*');
    }
}); 


zrouter.get("/sqltable/:sql", async (req, res) => {


    /*
        #swagger.description = "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table: <br>
        &emsp;<b>full:</b> Zabbix SKU search results <br>
        &emsp;<b>condenced:</b> Sku Condenced <br>
        &emsp;<b>probManagement:</b> Top Problems from the last 30 days <br>
        &emsp;<b>problems:</b> Problems in Zabbix  <br>
        &emsp;<b>problemsCount:</b> Count of problems grouped by severity <br>
    "
        #swagger.produces = ["text/html"]
        #swagger.responses[200] = {
            description: "Zabbix  urilizing back end sql queries instead of Zabbix REST calls returning an HTML Table",
            schema: {
                oneOf:[
                    {
                        $ref: "#/definitions/fullTable",
                    },
                    {
                        $ref: "#/definitions/condencedTable",
                    }
                ]
            }
         } 
    */
    const flatten = require('flat').flatten;
    const row = html => `<tr>\n${html}</tr>\n`,
        heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
        datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));
    const pool = mysql.createPool(zmConfig);

    if (zqueries[req.params.sql]) {
        pool.query(zqueries[req.params.sql], (error, results) => {
            if (!results) {
                res.json({ status: "Not found!" });
            } else {
                //res.json(results);
                res.send(`<table border="1">
         ${heading(results[0])}
          ${results.reduce((html, object) => (html + datarow(flatten(object))), '')}
        </table>`)
            }
        })
    } else res.send('no params *' + req.params.sql + '*');

});

zrouter.get('/apikey', async (req, res) => {
    // #swagger.ignore = true
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
    zabbixLogin().then(async function (response) {
        // handle success
        let apikey = response;
        res.send(apikey.data.result);
    });


});

// zrouter.get('/problems', async (req, res) => {
//     zabbixLogin().then(async function (response) {
//         let apikey = response.data.result
//         //async function zabbixProblems() {
//         //    try {
//         const config = {
//             jsonrpc: '2.0',
//             method: 'problem.get',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Accept': '*/*',
//                 'Host': 'zmonitoring.choicecloud.com',
//                 'Accept-Encoding': 'gzip, deflate, br',
//                 'Connection': 'keep-alive',
//             },
//             url: 'https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php',


//             data: {

//                 "jsonrpc": "2.0",
//                 "method": "problem.get",
//                 "id": 1,
//                 "params": {
//                     //                   output: 'extend',
//                     "output": [
//                         'eventid',
//                         'objectid',
//                         'clock',
//                         'ns',
//                         'name',
//                         'acknowledged',
//                         'priority',
//                         'severity'
//                     ],
//                     filter: {
//                         severity: (5),
//                         recent: 0,
//                         value: 1
//                     },
//                     //sortfield: "priority",
//                     sortorder: 'DESC',
//                     limit: 100,
//                     recent: false,
//                     selectSuppressionData: "extend",
//                     countOutput: true



//                 },
//                 auth: apikey,
//                 id: 1

//             },



//         }
//         console.log(apikey);
//         axios(config).then(function (response) { res.json(response.data); console.log(response); }).catch(function (error) { console.log(error); })

//     })

// })

// zrouter.get('/event-client', async (req, res) => {
//     const flatten = require('flat').flatten;
//     const row = html => `<tr>\n${html}</tr>\n`,
//         heading = object => row(Object.keys(object).reduce((html, heading) => (html + `<th>${heading}</th>`), '')),
//         datarow = object => row(Object.values(object).reduce((html, value) => (html + `<td>${value}</td>`), ''));

//     const { ZabbixClient } = require("zabbix-client")
//     const client1 = new ZabbixClient("https://zmonitoring.choicecloud.com/zabbix/api_jsonrpc.php")
//     const api = await client1.login("nocboard", "timeout")

//     const problems = await api.method("event.get")
//         .call({
//             source: 0,
//             object: 0,
//             output: "extend",
//             filter: {
//                 severity: (
//                     5
//                 ),
//                 r_eventid: 0,

//             },
//             //sortfield: "priority",
//             selectSuppressionData: 'extend',
//             sortorder: 'ASC',
//             limit: 10,
//             recent: "true",

//         })

//         .then(result => {
//             //   res.json(result)
//             res.send(`<table>
//          ${heading(result[0])}
//           ${result.reduce((html, object) => (html + datarow(flatten(object))), '')}
//         </table>`)
//         })
//         .catch(err => {
//             // err instanceof ZabbixResponseException; // true
//             console.log("this error", err.message); // Incorrect method "thismethod.doesnotexist"
//         })

// })


module.exports = zrouter